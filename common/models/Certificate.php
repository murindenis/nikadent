<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "certificate".
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $base_url
 * @property int $created_at
 */
class Certificate extends \yii\db\ActiveRecord
{
    /**
     * @var
     */

    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificate';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['name', 'path', 'base_url'], 'string', 'max' => 255],
            [['picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    public function getPicture($default = null)
    {
        return $this->path
            ? Yii::getAlias($this->base_url . '/' . $this->path)
            : $default;
    }
}
