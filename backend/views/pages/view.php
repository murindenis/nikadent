<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-view">

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'subcategory_id',
            'title',
            'slug',
            'sort',
            'description:ntext',
            'target:ntext',
            'active_ingredient:ntext',
            'material:ntext',
            'treaded_area:ntext',
            'indication:ntext',
            'contraindication:ntext',
            'technique:ntext',
            'difference_from_mesotherapy:ntext',
            'efficiency:ntext',
            'recommendation:ntext',
            'result:ntext',
            'duration_of_procedure:ntext',
            'created_at:datetime',
            'seo_title',
            'seo_description',
            'seo_keywords',
            'seo_h1',
            'active:boolean',
        ],
    ]) ?>

</div>
