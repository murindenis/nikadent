<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AboutBox */

$this->title = Yii::t('backend', 'Create About Box');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'About Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-box-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
