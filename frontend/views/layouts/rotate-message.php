<div class="rotate-message hidden-sm hidden-lg-up text-center">
    <img src="/img/media/rotate/tablet.png" alt="Переверните устройство" class="hidden-xs">
    <img src="/img/media/rotate/mobile.png" alt="Переверните устройство" class="hidden-md" width="96">
    <h3>Поверните ваше<br>устройство</h3>
</div>