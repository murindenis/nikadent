<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Index */

$this->title = 'Редактировать раздел';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Indices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="index-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
