<?php

use yii\db\Migration;

/**
 * Handles the creation of table `medical_services`.
 */
class m180312_130427_create_medical_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('medical_services', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'created_at'  => $this->integer(),
        ], $tableOptions);

        $this->insert('medical_services', [
            'title'       => 'МЕДУСЛУГИ',
            'description' => 'Современная стоматология и эстетическая медицина неразрывно связаны. Стремление смотреть на медицину как можно шире и использовать наиболее прогрессивные решения дают нам возможность предлагать своим пациентам все, чтобы они были здоровыми и красивыми!',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('medical_services');
    }
}
