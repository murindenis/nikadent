<?php

use yii\db\Migration;

/**
 * Handles adding subcategory to table `pages`.
 */
class m180420_063927_add_subcategory_id_column_to_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('pages', 'subcategory_id', $this->integer()->after('category_id'));

        $this->createIndex('idx-pages_subcategory_id', '{{%pages}}', 'subcategory_id');
        $this->addForeignKey('fk-pages_subcategory_id', '{{%pages}}', 'subcategory_id', '{{%pages}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('pages','fk-pages_subcategory_id');
        $this->dropIndex('pages', 'idx-pages_subcategory_id');

        $this->dropColumn('pages', 'subcategory_id');
    }
}
