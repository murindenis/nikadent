<?php

use yii\db\Migration;

/**
 * Handles the creation of table `index`.
 */
class m180313_082548_create_index_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('index', [
            'id'          => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'title'       => $this->string()->notNull(),
            'quote'       => $this->string()->notNull(),
            'author'      => $this->string(),
            'path'        => $this->string(),
            'base_url'    => $this->string(),
            'created_at'  => $this->integer(),

        ], $tableOptions);

        $this->createIndex('idx-index_category_id', '{{%index}}', 'category_id');
        $this->addForeignKey('fk-index_category_id', '{{%index}}', 'category_id', '{{%category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-index_category_id','index');
        $this->dropIndex('idx-index_category_id','index');
        $this->dropTable('index');
    }
}
