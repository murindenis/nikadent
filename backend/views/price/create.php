<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Price */

$this->title = 'Добавить цены';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Prices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
