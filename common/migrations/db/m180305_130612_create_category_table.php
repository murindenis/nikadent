<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories`.
 */
class m180305_130612_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('category', [
            'id'         => $this->primaryKey(),
            'parent_id'  => $this->integer()->notNull()->defaultValue(0),
            'name'       => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
