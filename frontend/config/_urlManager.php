<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],

        // Articles
        ['pattern' => 'article/index', 'route' => 'article/index'],
        ['pattern' => 'article/attachment-download', 'route' => 'article/attachment-download'],
        ['pattern' => 'article/<slug>', 'route' => 'article/view'],


        ['pattern' => 'pages/<slug>', 'route' => 'pages/index'],
        ['pattern' => 'test/<slug>', 'route' => 'pages/test'],
        ['pattern' => 'category/<slug>', 'route' => 'pages/category'],

        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']],

        ['pattern' => 'index.php',    'route' => 'site/index'],
        ['pattern' => 'pacients.php', 'route' => 'site/pacients'],
        ['pattern' => 'about.php',    'route' => 'site/about'],
        ['pattern' => 'feedback.php', 'route' => 'site/feedback'],
        ['pattern' => 'price.php',    'route' => 'site/price'],
        ['pattern' => 'contacts.php', 'route' => 'site/contacts'],

        ['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml'],
    ]
];
