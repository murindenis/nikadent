$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

    $('.target_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.target_remove').removeClass('b-no-active');
        $('.target').removeClass('b-no-active');
    });

    $('.target_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.target_add').removeClass('b-no-active');
        $('.target').addClass('b-no-active');
        $('.target .redactor-editor').html('');
        $('.target textarea').val('');
    });

    $('.active_ingredient_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.active_ingredient_remove').removeClass('b-no-active');
        $('.active_ingredient').removeClass('b-no-active');
    });

    $('.active_ingredient_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.active_ingredient_add').removeClass('b-no-active');
        $('.active_ingredient').addClass('b-no-active');
        $('.active_ingredient .redactor-editor').html('');
        $('.active_ingredient textarea').val('');
    });

    $('.material_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.material_remove').removeClass('b-no-active');
        $('.material').removeClass('b-no-active');
    });

    $('.material_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.material_add').removeClass('b-no-active');
        $('.material').addClass('b-no-active');
        $('.material .redactor-editor').html('');
        $('.material textarea').val('');
    });

    $('.treaded_area_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.treaded_area_remove').removeClass('b-no-active');
        $('.treaded_area').removeClass('b-no-active');
    });

    $('.treaded_area_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.treaded_area_add').removeClass('b-no-active');
        $('.treaded_area').addClass('b-no-active');
        $('.treaded_area .redactor-editor').html('');
        $('.treaded_area textarea').val('');
    });

    $('.indication_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.indication_remove').removeClass('b-no-active');
        $('.indication').removeClass('b-no-active');
    });

    $('.indication_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.indication_add').removeClass('b-no-active');
        $('.indication').addClass('b-no-active');
        $('.indication .redactor-editor').html('');
        $('.indication textarea').val('');
    });

    $('.contraindication_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.contraindication_remove').removeClass('b-no-active');
        $('.contraindication').removeClass('b-no-active');
    });

    $('.contraindication_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.contraindication_add').removeClass('b-no-active');
        $('.contraindication').addClass('b-no-active');
        $('.contraindication .redactor-editor').html('');
        $('.contraindication textarea').val('');
    });

    $('.technique_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.technique_remove').removeClass('b-no-active');
        $('.technique').removeClass('b-no-active');
    });

    $('.technique_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.technique_add').removeClass('b-no-active');
        $('.technique').addClass('b-no-active');
        $('.technique .redactor-editor').html('');
        $('.technique textarea').val('');
    });

    $('.difference_from_mesotherapy_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.difference_from_mesotherapy_remove').removeClass('b-no-active');
        $('.difference_from_mesotherapy').removeClass('b-no-active');
    });

    $('.difference_from_mesotherapy_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.difference_from_mesotherapy_add').removeClass('b-no-active');
        $('.difference_from_mesotherapy').addClass('b-no-active');
        $('.difference_from_mesotherapy .redactor-editor').html('');
        $('.difference_from_mesotherapy textarea').val('');
    });

    $('.efficiency_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.efficiency_remove').removeClass('b-no-active');
        $('.efficiency').removeClass('b-no-active');
    });

    $('.efficiency_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.efficiency_add').removeClass('b-no-active');
        $('.efficiency').addClass('b-no-active');
        $('.efficiency .redactor-editor').html('');
        $('.efficiency textarea').val('');
    });

    $('.recommendation_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.recommendation_remove').removeClass('b-no-active');
        $('.recommendation').removeClass('b-no-active');
    });

    $('.recommendation_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.recommendation_add').removeClass('b-no-active');
        $('.recommendation').addClass('b-no-active');
        $('.recommendation .redactor-editor').html('');
        $('.recommendation textarea').val('');
    });

    $('.result_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.result_remove').removeClass('b-no-active');
        $('.result').removeClass('b-no-active');
    });

    $('.result_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.result_add').removeClass('b-no-active');
        $('.result').addClass('b-no-active');
        $('.result .redactor-editor').html('');
        $('.result textarea').val('');
    });

    $('.duration_of_procedure_add').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.duration_of_procedure_remove').removeClass('b-no-active');
        $('.duration_of_procedure').removeClass('b-no-active');
    });

    $('.duration_of_procedure_remove').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('b-no-active');
        $('.duration_of_procedure_add').removeClass('b-no-active');
        $('.duration_of_procedure').addClass('b-no-active');
        $('.duration_of_procedure input').val('');
    });
});