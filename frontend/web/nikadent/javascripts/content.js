$(document).ready(function() {
	$(window).load(function() {
		$(".pre-loading").delay(5000).fadeOut('slow');
	});

	function initMainStory() {
		$('#mainStory').fullpage({
			resize: false,
			animateAnchor: false,
			scrollOverflow: true,
			fitToSection: true,
			fitToSectionDelay: 1000,

			//scrolling
			css3: true,
			scrollingSpeed: 1100,
			responsive: 900,
			fitSection: false,
			easing: 'easeInOutCubic',
			easingcss3: 'ease',
			// navigation: true,
			fadingEffect: true,
			fadingEffect: 'slides',
			continuousHorizontal: true,
			controlArrows: false,
			showActiveTooltip: true,
			slidesNavigation: true,
			slidesNavPosition: 'bottom',
			slideTooltips: ["Терапевтическая стоматология",
							"Ортопедическая стоматология",
							"Парадонтологическая стоматология",
							"Хирургическая стоматология",
							"Дентальная имплантация",
							"Ортодонтическая стоматология",
							"Детская стоматология",
							"Эстетическая стоматология",
							"Зуботехническая лаборатория"],
			slideTooltips2: ["Лазерная косметология",
							 "Аппаратная косметология",
							 "Инъекционная косметология",
							 "Терапевтическая косметология"],
			slideTooltips3: ["Массаж",
							 "Обертывание",
							 "Физиотерапия"],

			lazyLoading: true,

			//events
			onLeave: function(index, nextIndex, direction){
				if (direction ="")
				console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " +  direction);
			},
			afterLoad: function(anchorLink, index){
				console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index );
			},
			afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){
				console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
			},
			onSlideLeave: function(anchorLink, index, slideIndex, direction){
				console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
			},
			afterRender: function(){
				console.log("afterRender");
			},
			afterResize: function(){
				console.log("afterResize");
			}

		});
	}

	function initPacientStory() {
		$('#pacientStory').fullpage({
			resize: false,
			animateAnchor: false,
			scrollOverflow: true,
			fitToSection: true,
			fitToSectionDelay: 1000,

			//scrolling
			css3: true,
			scrollingSpeed: 1100,
			responsive: 900,
			fitSection: false,
			easing: 'easeInOutCubic',
			easingcss3: 'ease',
			// navigation: true,
			fadingEffect: true,
			controlArrows: false,
			showActiveTooltip: true,
			slidesNavigation: true,
			slidesNavPosition: 'bottom',

			lazyLoading: true,

			//events
			onLeave: function(index, nextIndex, direction){
				if (direction ="")
				console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " +  direction);
			},
			afterLoad: function(anchorLink, index){
				console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index );
			},
			afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){
				console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
			},
			onSlideLeave: function(anchorLink, index, slideIndex, direction){
				console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
			},
			afterRender: function(){
				console.log("afterRender");
			},
			afterResize: function(){
				console.log("afterResize");
			}
		});
	}

	function initAboutStory() {
		$('#aboutStory').fullpage({
			resize: false,
			animateAnchor: false,
			scrollOverflow: true,
			fitToSection: true,
			fitToSectionDelay: 1000,

			//scrolling
			css3: true,
			scrollingSpeed: 1100,
			responsive: 900,
			fitSection: false,
			easing: 'easeInOutCubic',
			easingcss3: 'ease',
			// navigation: true,
			fadingEffect: true,
			controlArrows: false,
			showActiveTooltip: true,
			slidesNavigation: true,
			slidesNavPosition: 'bottom',
			slideTooltips6: ["Гарантия профессионализма",
							 "Гарантии клинической безопасности",
							 "Гарантия на лечение",
							 "Гарантии правовой безопасности",
							 "Гарантия инфекционной безопасности",
							 "Гарантия инфекционной безопасности"],
			lazyLoading: true,

			//events
			onLeave: function(index, nextIndex, direction){
				if (direction ="")
				console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " +  direction);
			},
			afterLoad: function(anchorLink, index){
				console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index );
			},
			afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){
				console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
			},
			onSlideLeave: function(anchorLink, index, slideIndex, direction){
				console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
			},
			afterRender: function(){
				console.log("afterRender");
			},
			afterResize: function(){
				console.log("afterResize");
			}
		});
	}

	function initContactStory() {
		$('#contactContainer').fullpage({
			resize: false,
			animateAnchor: false,
			scrollOverflow: true,
			fitToSection: true,
			fitToSectionDelay: 1000,

			//scrolling
			css3: true,
			scrollingSpeed: 1100,
			responsive: 900,
			fitSection: false,
			easing: 'easeInOutCubic',
			easingcss3: 'ease',
			// navigation: true,
			fadingEffect: true,
			controlArrows: false,
			showActiveTooltip: true,
			slidesNavigation: true,
			slidesNavPosition: 'bottom',

			lazyLoading: true,

			//events
			onLeave: function(index, nextIndex, direction){
				if (direction ="")
				console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " +  direction);
			},
			afterLoad: function(anchorLink, index){
				console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index );
			},
			afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){
				console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
			},
			onSlideLeave: function(anchorLink, index, slideIndex, direction){
				console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
			},
			afterRender: function(){
				console.log("afterRender");
			},
			afterResize: function(){
				console.log("afterResize");
			}
		});
	}

	initMainStory();
	initPacientStory();
	initAboutStory();
	initContactStory();

	$('.moveSectionDown').click(function(e){
		e.preventDefault();
		$.fn.fullpage.moveSectionDown();
	});

	$('.moveSlideRight').click(function(e){
		e.preventDefault();
		$.fn.fullpage.moveSlideRight();
	});

	$('.moveSlideLeft').click(function(e){
		e.preventDefault();
		$.fn.fullpage.moveSlideLeft();
	});

	/**
	 * 	Custom select
	 */
	// $('select').awselect();

	var acc = document.getElementsByClassName("s-link-list");
	var i;

	for ( i = 0; i < acc.length; i++ ) {
		acc[i].onclick = function() {
			this.classList.toggle("active");

			var panel = this.nextElementSibling;

			if (panel.style.maxHeight){
				panel.style.maxHeight = null;
			} else {

				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		}
	}

	var accA = document.getElementsByClassName("content-section");
	var j;

	for ( j = 0; j < accA.length; j++ ) {
		accA[j].onclick = function() {
			this.classList.toggle("active");

			var panel = this.nextElementSibling;

			if (panel.style.maxHeight){
				panel.style.maxHeight = null;
			} else {
				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		}
	}

	$('.js-contacts-link').click(function() {
		$('.contacts-popup').removeClass('hidden');
	});

    $('.js-menu-link').on('click', function (e) {
        e.preventDefault();
        $('.side-navigation,.js-menu-burger,.js-menu-close').toggleClass('hidden');
        // $('.js-menu-burger').addClass('hidden');
        // $('.js-menu-close').removeClass('hidden');
    });

    $('.nav__item_parent').on('mouseenter', function (e) {
        e.preventDefault();
        $('.submenu').addClass('active');
    });

    $('.nav__item').on('mouseenter', function (e) {
        if (!$(this).hasClass('nav__item_parent')) {
            $('.submenu').removeClass('active');
        }
    });

    $('.submenu').on('mouseleave', function (e) {
        $(this).removeClass('active');
    });

	$(".side-menu a.s-menu").click( function () {
		var el = $(this);
		el.parents('aside').toggleClass("hidden");
		$('.js-menu-close').addClass('hidden');
		$('.js-menu-burger').removeClass('hidden');	
	});

	$('.js-feedbacks-link').click(function() {
		$('.feedbacks-popup').removeClass('hidden');
	});

	$('.js-feedbacks-close').click(function(){
		$('.feedbacks-popup').addClass('hidden');
	});
	
	$('.js-contacts-close').click(function(){
		$('.contacts-popup').addClass('hidden');
	});

	function recordForm(form, nameForm, phoneForm, dateForm, doctorForm, msgForm, contactsFileds, contactsFooter) {
        if (nameForm == '') {
            msgForm.removeClass('hidden');
            msgForm.html('Обратите внимание, введите ваше имя');
        }
        if (phoneForm == '') {
            msgForm.removeClass('hidden');
            msgForm.html('Обратите внимание, введите ваш номер телефона');
        }
        if (doctorForm = '') {
            msgForm.removeClass('hidden');
            msgForm.html('Обратите внимание, выберите доктора');
        }
        if (dateForm == '') {
            msgForm.removeClass('hidden');
            msgForm.html('Обратите внимание, введите дату');
        }
        if (nameForm != '' && phoneForm != '' && dateForm != '') {
            $.ajax({
                type: 'post',
                data: form.serialize(),
                url: '/site/form-ajax',
                success: function (data) {
                    contactsFileds.addClass('hidden');
                    contactsFooter.addClass('hidden');

                    setTimeout(showSuccessSending, 500);

                    function showSuccessSending(){
                        msgForm.removeClass('hidden');
                        msgForm.html('<h3>Ваша заявка принята</h3>\n' +
                            '<p>Мы свяжемся с вами в ближайшее время</p>');
                    }

                    setTimeout(resetForm, 7000);

                    function resetForm(){
                        form.trigger( 'reset' );

                        contactsFileds.removeClass('hidden');
                        contactsFooter.removeClass('hidden');
                        msgForm.addClass('hidden');
                    }
                }
            });
        }
	}

	/**
	 * Using sendMail() for contact form.
	 */
	function sendMail() {

		var submitFormBtn = $('.submitBtn');

		submitFormBtn.on('click', function (e) {
			e.preventDefault();
			var form            = $('#contact'),
				nameForm        = $("#contact input[name='name']").val(),
				phoneForm       = $("#contact input[name='phone']").val(),
				dateForm        = $("#contact input[name='date']").val(),
				doctorForm		= $("#contact doctor").val(),
				msgForm         = $('.js-contacts-errors'),
				contactsFileds  = $('.js-contacts-form'),
				contactsFooter  = $('.js-contacts-footer');

            recordForm(form, nameForm, phoneForm, dateForm, doctorForm, msgForm, contactsFileds, contactsFooter);
		});
	}

    function categorySendMail() {

        var submitFormBtn = $('.category__submitBtn');

        submitFormBtn.on('click', function (e) {
            e.preventDefault();
            var form            = $('#category__contact'),
                nameForm        = $("#category__contact input[name='name']").val(),
                phoneForm       = $("#category__contact input[name='phone']").val(),
                dateForm        = $("#category__contact input[name='date']").val(),
                doctorForm		= $("#category__contact doctor").val(),
                msgForm         = $('.js-contacts-errors'),
                contactsFileds  = $('.js-contacts-form'),
                contactsFooter  = $('.js-contacts-footer');

            recordForm(form, nameForm, phoneForm, dateForm, doctorForm, msgForm, contactsFileds, contactsFooter);
        });
    }

	sendMail();
	categorySendMail();

	function sendReview() {
		var addComment = $('.addComment');

			addComment.on('click', function(e){
				e.preventDefault();

				var commentsForm    = $('#comments'),
					form            = $('#comment-form'),
					phoneForm       = $("#comment-form input[name='phone']").val(),
					nameForm        = $("#comment-form input[name='name']").val(),
					titleForm       = $("#comment-form input[name='title']").val(),
					textForm        = $("#comment-form textarea[name='text']").val(),
					dateForm        = $("#comment-form input[name='date']").val(),
					msgForm         = $('.js-feedbacks-errors'),
					feedbackFileds  = $('.js-feedbacks-form'),
					feedbackFooter  = $('.js-feedbacks-footer');

				if (phoneForm == '') {
					msgForm.removeClass('hidden');
					msgForm.html('Обратите внимание, введите ваш телефон');
				}

				if (nameForm == '') {
					msgForm.removeClass('hidden');
					msgForm.html('Обратите внимание, введите ваше имя');
				}

				// if (titleForm == '') {
				// 	msgForm.removeClass('hidden');
				// 	msgForm.html('Обратите внимание, введите тему');
				// }

				if (textForm == '') {
					msgForm.removeClass('hidden');
					msgForm.html('Обратите внимание, введите отзыв');
				}
				// if (nameForm != '' && titleForm != '' && textForm != '' && phoneForm != '') {
				if (nameForm != '' && textForm != '' && phoneForm != '') {

					$.ajax({
                        url: '/site/feedback-ajax',
						type: 'post',
						data: form.serialize(),
						success: function (data) {
							feedbackFileds.addClass('hidden');
							feedbackFooter.addClass('hidden');
	
							setTimeout(showSuccessSending, 500);
	
							function showSuccessSending(){
								commentsForm.prepend('<div class="comment col-xs-12 col-md-12 col-lg-6 middle-md">' + 
														'<div class="feedback-comment ">' + 
															'<div class="hidden">'+titleForm+'</div>' + 
															'<div class="feedback-text">'+textForm+'</div>' + 
															'<div class="feedback-footer">' + 
																'<div class="feedback-footer__name">'+nameForm+'</div>' + 
																'<div class="feedback-footer__date">'+dateForm+'</div>' +
															'</div>' + 
														'</div>' +
													'</div>');
								msgForm.removeClass('hidden');
								msgForm.html('<h3>Ваш отзыв принят</h3>\n' +
											 '<p>Спасибо за доверие!</p>');
							}
	
							setTimeout(resetForm, 7000);
	
							function resetForm(){
								form.trigger( 'reset' );
	
								feedbackFileds.removeClass('hidden');
								feedbackFooter.removeClass('hidden');
								msgForm.addClass('hidden');
							}
						}
					});
				}
			});
	}

	sendReview();

    // Using DateTimePicker jQuery plugin for contact form.
    $('#datepicker').datetimepicker({
        lang: 'ru',
        // defaultTime: '10:00',
        format: 'd-m-Y H:i',
        //minTime: '8:00',
		//maxTime: '20:30',
		 allowTimes:[
		 	'08:00', '08:30', '09:00', '09:30', '10:00', '10:30',
		 	'11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
		 	'14:00', '14:30', '15:00', '15:30', '16:00', '16:30',
		 	'17:00', '17:30', '18:00', '18:30', '19:00', '19:30',
		 	'20:00', '20:30'
		    ],
        //step:'30'
	});

    // Using DateTimePicker jQuery plugin for contact form.
    $('#category__datepicker').datetimepicker({
        lang: 'ru',
        // defaultTime: '10:00',
        format: 'd-m-Y H:i',
        //minTime: '8:00',
        //maxTime: '20:30',
        allowTimes:[
            '08:00', '08:30', '09:00', '09:30', '10:00', '10:30',
            '11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
            '14:00', '14:30', '15:00', '15:30', '16:00', '16:30',
            '17:00', '17:30', '18:00', '18:30', '19:00', '19:30',
            '20:00', '20:30'
        ],
        //step:'30'
    });
	
	//$('select').awselect();

	$('.fixed-link-list').on('click', function () {
		$(this).closest('.main__menu__item').siblings('.main__menu__item').find('.fixed-link-list').removeClass('active');
		$(this).closest('.main__menu__item').siblings('.main__menu__item').find('.side-sub-menu').slideUp();
		$(this).toggleClass('active');
		$(this).siblings('.side-sub-menu').slideToggle();
	})
});