<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ApplicationForDoctorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Application For Doctors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-for-doctor-index">

    <p>
        <?= Html::a(Yii::t('backend', 'Create Application For Doctor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($data) {
            if ($data->status) {
                return ['class' => 'success'];
            }
        },
        'columns' => [

            'id',
            'username',
            'phone',
            [
                'attribute' => 'doctor_id',
                'value'     => 'doctor.name',
                'filter' => Html::activeDropDownList($searchModel, 'doctor_id', \yii\helpers\ArrayHelper::map(\common\models\Doctor::find()->all(), 'id', 'name'), ['prompt' => '-', 'class' => 'form-control']),

            ],
            'date:datetime',
            //'comment:ntext',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
