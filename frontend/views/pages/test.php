<?php
$this->title = $model->seo_title;

$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $model->seo_description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $model->seo_title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $model->seo_keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $model->seo_description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://nikadent.by/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ÎÎÎ ÀéÒè Òåõíîëîäæèñ Ãðóïï'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'    => 'DC.title',
    'content' => $model->seo_title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $model->seo_description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
?>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header-back.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/feedback-popup.php');
    ?>
    <div class="main" id="new__page">
        <div class="" id="article-1">
            <div class="article-section">

                <div class="article-row">
                    <div class="fixed-menu-container">
                    <div class="fixed-menu-wrapper">
                        <ul>
                            <li class="main__menu__item">
                                <div class="fixed-link-list">
                                    <div class="icon-menu">
                                        <div class="fa fa-chevron-down rotate"></div>
                                    </div>
                                    <a href="#" class="fixed-link-drop s-link-red">Стоматология</a>
                                </div>
                                <div class="side-sub-menu">
                                    <ul>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/0">Терапевтическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/1">Ортопедическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/2">Парадонтологическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/3">Хирургическая стоматология</a></li>
                                        <li class="active"><a class="s-menu s-link-sm" href="/index.php#4/4">Дентальная имплантация</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/5">Ортодонтическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/6">Детская стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/7">Эстетическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/8">Зуботехническая лаборатория</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__menu__item">
                                <div class="fixed-link-list">
                                    <div class="icon-menu">
                                        <div class="fa fa-chevron-down rotate"></div>
                                    </div>
                                    <a href="#" class="fixed-link-drop s-link-red">Стоматология</a>
                                </div>
                                <div class="side-sub-menu">
                                    <ul>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/0">Терапевтическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/1">Ортопедическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/2">Парадонтологическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/3">Хирургическая стоматология</a></li>
                                        <li class="active"><a class="s-menu s-link-sm" href="/index.php#4/4">Дентальная имплантация</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/5">Ортодонтическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/6">Детская стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/7">Эстетическая стоматология</a></li>
                                        <li><a class="s-menu s-link-sm" href="/index.php#4/8">Зуботехническая лаборатория</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                    <div class="row article-title">
                        <h1><?= $model->title?></h1>
                    </div>
                    <div class="article-side">
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->description?>
                            </div>
                        </div>
                    </div>
                    <? if ($model->target) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Öåëü</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->target?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->active_ingredient) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Àêòèâíûé êîìïîíåíò</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->active_ingredient?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->material) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Ìàòåðèàëû</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->material?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->treaded_area) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Îáðàáàòûâàåìûå çîíû</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->treaded_area?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->indication) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Ïîêàçàíèÿ</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->indication?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->contraindication) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Ïðîòèâîïîêàçàíèÿ</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->contraindication?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->technique) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Ìåòîäèêà è ýòàïíîñòü ïðîöåäóðû</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->technique?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->efficiency) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Ýôôåêòèâíîñòü</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->efficiency?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->recommendation) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Ðåêîìåíäàöèè äî è ïîñëå ïðîöåäóðû</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->recommendation?>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                    <? if ($model->result) {?>
                    <div class="article-side">
                        <div class="row article-row-title">Ðåçóëüòàò</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $model->result?>
                                <br>
                                <hr class="hr-article">
                                <p>Ïðîäîëæèòåëüíîñòü ïðîöåäóðû <?= $model->duration_of_procedure?></p>
                            </div>
                        </div>
                    </div>
                    <? } ?>
                </div>
            </div>
</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>
