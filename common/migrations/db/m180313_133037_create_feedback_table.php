<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m180313_133037_create_feedback_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('feedback', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(),
            'phone'      => $this->string(),
            'theme'      => $this->string(),
            'text'       => $this->text(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('feedback');
    }
}
