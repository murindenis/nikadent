<?php

use yii\db\Migration;

/**
 * Class m180312_145314_add_index_to_category_table
 */
class m180312_145314_add_index_to_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createIndex('idx-pages_category_id', '{{%pages}}', 'category_id');
        $this->addForeignKey('fk-pages_category_id', '{{%pages}}', 'category_id', '{{%category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('pages', 'fk-pages_category_id');
        $this->dropIndex('pages', 'idx-pages_category_id');
    }
}
