<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m180313_102726_create_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('contacts', [
            'id'          => $this->primaryKey(),
            'address'     => $this->string(),
            'phone_house' => $this->string(),
            'phone_mob1'  => $this->string(),
            'phone_mob2'  => $this->string(),
            'email'       => $this->string(),
            'work_time'   => $this->string(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contacts');
    }
}
