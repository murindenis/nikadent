<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "application_for_doctor".
 *
 * @property int $id
 * @property string $username
 * @property string $phone
 * @property int $doctor_id
 * @property int $date
 * @property string $comment
 * @property int $created_at
 * @property boolean $status
 *
 * @property Doctor $doctor
 */
class ApplicationForDoctor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application_for_doctor';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'phone', 'doctor_id', 'date'], 'required'],
            [['doctor_id', 'date', 'created_at'], 'integer'],
            [['comment'], 'string'],
            [['username', 'phone'], 'string', 'max' => 255],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'id']],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'username' => Yii::t('common', 'Username'),
            'phone' => Yii::t('common', 'Phone'),
            'doctor_id' => Yii::t('common', 'Name Doctor'),
            'date' => Yii::t('common', 'Date'),
            'comment' => Yii::t('common', 'Comment'),
            'created_at' => Yii::t('common', 'Created At'),
            'status' => 'Заявка обработана',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['id' => 'doctor_id']);
    }

    public function getDoctorColumn() {
        return Doctor::find()
            ->select(['name', 'id'])
            ->indexBy('id')
            ->column();
    }

    public function sendEmail($from, $to, $subject, $text) {
        return Yii::$app->mailer->compose()
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->setTextBody($text)
            //->setHtmlBody('<b>текст сообщения в формате 11111HTML11111</b>')
            ->send();
    }
}
