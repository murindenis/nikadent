<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/seo.php');
?>
<div id="panel"></div>
<div id="outdated"></div>
    <h1 style="display:none;">
        <?= $seo->h1 ?>
    </h1>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    ?>

    <div id="aboutStory">
        <div class="section" id="s-about-title">
            <div class="section-content section-content-bottom">
                <img src="/img/media/chapters/a-ch-1.jpg" data-src="/img/media/chapters/a-ch-1.jpg" alt="" data-object-fit="cover" data-object-position="50% 50%">
                <article class="">
                    <div class="container-bottom">
                        <div class="brushes">
                            <div class="brush brush-white">
                                <h3>О клинике</h3>
                            </div>
                        </div>
                        <div class="link-more link-more-inner hidden-sm-down">
                            <a class="btn btn-primary btn-square moveSectionDown" href="#">
                                <svg class="icon icon-arrow-down" width="7" height="32">
                                    <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                                    <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                                    <rect class="arrow-outter" x="3" width="1" height="10"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <div class="section" id="s-about-0">
            <div class="section-content section-content-center">
                <img src="<?= $aboutBox->getPicture() ?>"
                     data-src="<?= $aboutBox->getPicture() ?>"
                     alt=""
                     data-object-fit="scale-down"
                     data-object-position="50% 50%">
                <article class="text-center">
                    <h2 class="grad"><?= $aboutBox->title?></h2>
                    <p class="lead text-lead">
                        <?= $aboutBox->description?>
                    </p>
                    <p class="lead text-lead"><b class="grad">ДОБРО ПОЖАЛОВАТЬ!</b></p>
                </article>
                <div class="link-more link-more-inner hidden-sm-down">
                    <a class="btn btn-primary btn-square moveSectionDown" href="#">
                        <svg class="icon icon-arrow-down" width="7" height="32">
                            <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                            <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                            <rect class="arrow-outter" x="3" width="1" height="10"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="section" id="s-mission-0">
            <div class="row">
                <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                    <article class="content-desc">
                        <div class="g-item">
                            <div class="g-content">
                                <h3>Миссия клиники</h3>
                                <p>Повышать качество жизни, помогая стать людям здоровыми, сохраняя молодость и красоту</p>
                            </div>
                            <div class="g-content">
                                <h3>Наша задача</h3>
                                <p>Помогать с любовью к людям</p>
                            </div>
                            <div class="g-content">
                                <h3>Главный принцип работы</h3>
                                <p>Совершенство каждой детали: из них складывается общий результат работы</p>
                            </div>
                            <div class="g-content">
                                <h3>Главная ценность</h3>
                                <p>Наш профессионализм и доверие пациентов</p>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                    <img src="/img/media/about/about-1.png" data-src="/img/media/about/about-1.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">
                </div>
                <div class="link-more link-more-inner hidden-sm-down">
                    <a class="btn btn-primary btn-square moveSectionDown" href="#">
                        <svg class="icon icon-arrow-down" width="7" height="32">
                            <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                            <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                            <rect class="arrow-outter" x="3" width="1" height="10" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="section" id="s-numbers-0">
            <div class="screen-content">
                <article class="content-desc screen-content-center screen-transition-animation-content">
                    <div class="mission-section">
                    </div>
                    <div class="row">
                        <div class="col col-xl-3 col-lg-3 col-md-12 col-xs-12">
                            <div class="mission-item">
                                <div class="mission-title lg"><750</div>
                                <div class="mission-desc light">Научных исследований свидетельствуют о клинической безопасности технологий, используемых в «НИКА Дент»</div>
                            </div>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col col-xl-3 col-lg-3 col-md-12 col-xs-12">
                            <div class="mission-item">
                                <div class="mission-title dark-lg">97%</div>
                                <div class="mission-desc dark">Результативность работ и долговечность</div>
                            </div>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col col-xl-3 col-lg-3 col-md-12 col-xs-12">
                            <div class="mission-item">
                                <div class="mission-title">2002</div>
                                <div class="mission-desc light">С 2002 года создаем новое качество жизни</div>
                            </div>
                        </div>
                        <div class="col col-xl-1 col-lg-1 col-md-12 col-xs-12"></div>

                        <div class="col col-xl-3 col-lg-3 col-md-12 col-xs-12">
                            <div class="mission-item">
                                <div class="mission-title">10</div>
                                <div class="mission-desc light">В клинике 10 опытных врачей</div>
                            </div>
                        </div>
                        <div class="col col-xl-3 col-lg-3 col-md-12 col-xs-12">
                            <div class="mission-item">
                                <div class="mission-title dark">3</div>
                                <div class="mission-desc dark">Уровня гарантии инфекционной безопасности</div>
                            </div>
                        </div>

                        <div class="col col-xl-1 col-lg-1 col-md-12 col-xs-12"></div>
                        <div class="col col-xl-2 col-lg-2 col-md-12 col-xs-12">
                            <div class="mission-item">
                                <div class="mission-title">15</div>
                                <div class="mission-desc light">Опыт врачей<br class="hidden-sm-down"> более 15 лет</div>
                            </div>
                        </div>
                        <div class="col col-xl-3 col-lg-3 col-md-12 col-xs-12">
                            <div class="mission-item">
                                <div class="mission-title dark">10.000</div>
                                <div class="mission-desc dark">Наши технологии лечения помогли более 10000 человек</div>
                            </div>
                        </div>
                    </div>
                </article>
                <div class="link-more link-more-inner hidden-sm-down">
                    <a class="btn btn-primary btn-square moveSectionDown" href="#">
                        <svg class="icon icon-arrow-down" width="7" height="32">
                            <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                            <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                            <rect class="arrow-outter" x="3" width="1" height="10" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="section" id="s-mission-1">
            <div class="row">
                <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                    <article class="content-desc screen-transition-animation-content">
                        <h3>Наши ценности и гарантии</h3>
                        <p>Главная ценность - наш профессионализм и доверие пациента. Мы делаем все для вашего комфорта, безопасности и уверенности в качестве лечения. </p>
                        <ul>
                            <li>Гарантии профессионализма</li>
                            <li>Гарантии клинической безопасности</li>
                            <li>Гарантия на лечение</li>
                            <li>Гарантии правовой безопасности</li>
                            <li>Гарантия инфекционной безопасности</li>
                            <li>Гарантия инфекционной безопасности</li>
                        </ul>
                    </article>
                </div>
                <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                    <img src="/img/media/about/guaranty-7.png" data-src="/img/media/about/guaranty-7.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">
                </div>
            </div>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                        <rect class="arrow-outter" x="3" width="1" height="10" />
                    </svg>
                </a>
            </div>
        </div>

        <div class="section" id="s-guaranty-0">
            <div class="slide" id="a-slide-1">
                <div class="row">
                    <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                        <article class="content-desc screen-transition-animation-content">
                            <h3>Гарантия профессионализма</h3>
                            <p>Все врачи клиники «НИКА Дент» имеют:</p>
                            <ul>
                                <li>дипломы и сертификаты по сециальности и подтверждают их в соответствии с требованиями законодательства</li>
                                <li>регулярно проходят аттестацию, удостоверяющую их врачебную категорию</li>
                                <li>участвуют в конференциях, семинарах и выставках в рамках страны и зарубежом</li>
                            </ul>
                        </article>
                    </div>
                    <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                        <img src="/img/media/about/guaranty-8.png" data-src="/img/media/about/guaranty-8.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">
                    </div>
                </div>
            </div>
            <div class="slide" id="a-slide-2">
                <div class="row">
                    <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                        <article class="content-desc screen-transition-animation-content">
                            <h3>Гарантии клинической безопасности</h3>
                            <p>Клиническая безопасность пациента определяется правильной и своевременной диагностикой патологии и применением адекватных лечебных мероприятий в целях достижения благополучного результата.</p>
                        </article>
                    </div>
                    <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                        <img src="/img/media/about/guaranty-2.png" data-src="/img/media/about/guaranty-2.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">
                    </div>
                </div>
            </div>
            <div class="slide" id="a-slide-3">
                <div class="row">
                    <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                        <article class="content-desc screen-transition-animation-content">
                            <h3>Гарантия на лечение</h3>
                            <p>Клиника «НИКА Дент» предоставляет для своих пациентов гарантийные сроки и сроки службы на производимые в клинике медицинские услуги, определяет виды гарантийных случаев, порядок их устранения.</p>
                        </article>
                    </div>
                    <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                        <img src="/img/media/about/guaranty-3.png" data-src="/img/media/about/guaranty-3.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">
                    </div>
                </div>
            </div>
            <div class="slide" id="a-slide-4">
                <div class="row">
                    <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                        <article class="content-desc screen-transition-animation-content">
                            <h3>Гарантии правовой безопасности</h3>
                            <p>В клинике «НИКА Дент» лечение пациентов осуществляется согласно имеющимся лицензиям на все виды медицинских услуг. С каждым пациентом перед началом лечения заключается договор на оказание медицинских услуг.</p>
                        </article>
                    </div>
                    <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                        <img src="/img/media/about/guaranty-4.png" data-src="/img/media/about/guaranty-4.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">
                    </div>
                </div>
            </div>
            <div class="slide" id="a-slide-5">
                <div class="row">
                    <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                        <article class="content-desc screen-transition-animation-content">
                            <h3>Гарантия инфекционной безопасности</h3>
                            <ul>
                                <li>Прием каждого пациента осуществляется <b><u>только стерильными индивидуальными и одноразовыми инструментами.</u></b></li>
                                <li>Медицинские инструменты, после каждого пациента подвергаются <b><u>3 этапам обработки</u></b>:
                                    <br>1. дезинфекции,
                                    <br>2. предстерилизационной очистке,
                                    <br>3. стерилизации.</li>
                            </ul>
                        </article>
                    </div>
                    <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                        <img src="/img/media/about/guaranty-5.png" data-src="/img/media/about/guaranty-5.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">
                    </div>
                </div>
            </div>
            <div class="slide" id="a-slide-6">
                <div class="row">
                    <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                        <article class="content-desc screen-transition-animation-content">
                            <ul>
                                <h3>Гарантия инфекционной безопасности</h3>
                                <p><b><u>Стерильный воздух - основа клинической безопасности</u></b></p>
                                <li>3-х уровневая очистка воздуха</li>
                                <li>обработка воздуха антибактериальным-фильтром</li>
                                <li>воздух поступает по медному трубопроводу из «сердца клиники» - собственной компрессорной.</li>
                                <p></p>
                                <p class="small">Благодаря стерильному воздуху и другим факторам сводим к минимуму осложнения как во время операции, так и в процессе лечения зуба. В противном случае, для пациента и врача существует угроза: при открытых ранах возникнет серьезная инфекция, а живой зуб может погибнуть.</p>
                            </ul>
                        </article>
                    </div>
                    <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                        <img src="/img/media/about/guaranty-52.png" data-src="/img/media/about/guaranty-52.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%">

                    </div>
                </div>
            </div>
            <div class="secondary-navigation">
                <div>
                    <a class="btn btn-lg btn-secondary btn-prev moveSlideLeft" href="#">
                        <svg class="icon icon-arrow-left" width="31" height="7">
                            <path class="arrow-inner" d="M1206,1151.5l6,3.5v-7l-6,3.5" transform="translate(-1206 -1148)"/>
                            <rect class="arrow-inner" x="6" y="3" width="15" height="1"/>
                            <rect class="arrow-outter" x="21" y="3" width="10" height="1"/>
                        </svg>
                        <span class="btn-lbl hidden-sm-down">Предыдущий</span>
                    </a>
                </div>
                <div>
                    <a class="btn btn-lg btn-secondary btn-next moveSlideRight" href="#">
                        <svg class="icon icon-arrow-right" width="31" height="7">
                            <path class="arrow-inner" d="M1242,1151.5l-6,3.5v-7l6,3.5" transform="translate(-1211 -1148)"/>
                            <rect class="arrow-inner" x="10" y="3" width="15" height="1"/>
                            <rect class="arrow-outter" y="3" width="10" height="1"/>
                        </svg>
                        <span class="btn-lbl hidden-sm-down">Следующий</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="section">
            <?php foreach ($certificate as $item) {?>
                <div class="slide">
                    <div class="row">
                        <div class="col col-md-12 section-content">
                            <img src="<?= $item->getPicture() ?>"
                                 data-src="<?= $item->getPicture() ?>"
                                 alt=""
                                 data-object-fit="scale-down"
                                 data-object-position="0 50%"
                                 class="service-preview">
                        </div>
                    </div>
                </div>
            <? } ?>

            <div class="secondary-navigation">
                <div>
                    <a class="btn btn-lg btn-secondary btn-prev moveSlideLeft" href="#">
                        <svg class="icon icon-arrow-left" width="31" height="7">
                            <path class="arrow-inner" d="M1206,1151.5l6,3.5v-7l-6,3.5" transform="translate(-1206 -1148)"/>
                            <rect class="arrow-inner" x="6" y="3" width="15" height="1"/>
                            <rect class="arrow-outter" x="21" y="3" width="10" height="1"/>
                        </svg>
                        <span class="btn-lbl hidden-sm-down">Предыдущий</span>
                    </a>
                </div>
                <div>
                    <a class="btn btn-lg btn-secondary btn-next moveSlideRight" href="#">
                        <svg class="icon icon-arrow-right" width="31" height="7">
                            <path class="arrow-inner" d="M1242,1151.5l-6,3.5v-7l6,3.5" transform="translate(-1211 -1148)"/>
                            <rect class="arrow-inner" x="10" y="3" width="15" height="1"/>
                            <rect class="arrow-outter" y="3" width="10" height="1"/>
                        </svg>
                        <span class="btn-lbl hidden-sm-down">Следующий</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>