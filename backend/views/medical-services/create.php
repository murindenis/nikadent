<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MedicalServices */

$this->title = Yii::t('backend', 'Create Medical Services');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Medical Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-services-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
