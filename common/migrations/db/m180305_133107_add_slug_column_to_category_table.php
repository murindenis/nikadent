<?php

use yii\db\Migration;

/**
 * Handles adding slug to table `category`.
 */
class m180305_133107_add_slug_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'slug', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('category', 'slug');
    }
}
