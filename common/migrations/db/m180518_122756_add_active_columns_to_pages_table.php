<?php

use yii\db\Migration;

/**
 * Handles adding active to table `pages`.
 */
class m180518_122756_add_active_columns_to_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('pages', 'active', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('pages', 'active');
    }
}
