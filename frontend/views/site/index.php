<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/seo.php');
?>
<h1 style="display:none;">
    <?= $seo->h1 ?>
</h1>
<p class="hidden-xs hidden-sm hidden-md hidden-lg hidden-xl ">
    Медицинский центр «НИКА Дент» в Бресте осуществляет следующие виды медицинских услуг: стоматология (детская и взрослая),
    ренгенодиагностика, отоларингология (ЛОР), косметология (лазерная, аппаратная, инъекционная, терапевтическа), дерматология, онкология,
    массаж (лечебный,сегментарный, лимфодренажный, антицеллюлитный, медовый, ароматерапевтический) и реабилитация.
</p>
<!--<div class="site-index"></div>-->
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    ?>

    <div class="main" id="mainStory">
        <div class="section" id="s-intro">
            <div class="sub-section">
                <img src="/img/media/main/background.png" data-src="/img/media/main/background.png" alt="Медицинский центр «НИКА Дент» Брест" data-object-fit="cover" data-object-position="50% 50%">
                <div class="brush-idea brush-cloud">
                    <div class="cloud-desc">
                        Доверяю<br>самое главное…
                    </div>
                </div>
                <div class="container-bottom section-content section-content-bottom">
                    <article class="content-brush no-overflow">
                        <div class="brushes no-overflow">
                            <div class="brush brush-yellow">
                                <!--<p><a href="/upload/k-01-2018.pdf" target="_blank" class="hot-link">Примите участие в проекте!</a></p>-->
                                <!--<p><a href="https://www.instagram.com/nikadent.by/">Примите участие в отбеливании</a></p>-->
                                <p>Отбеливание зубов Beyond,США</p>
                            </div>
                            <div class="brush brush-green">
                                <p>New! Лазерная терапия Candela, США!</p>
                            </div>
                            <div class="brush brush-red">
                                <p>Эндоскопическая диагностика ЛОР-органов Heinemann</p>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="link-more link-intro hidden-sm-down">
                    <a class="btn btn-primary btn-square moveSectionDown" href="#">
                        <svg class="icon icon-arrow-down" width="7" height="32">
                            <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                            <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                            <rect class="arrow-outter" x="3" width="1" height="10"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="section" id="s-medservice-ch">
            <img src="<?= $medicalServices->getPicture() ?>"
                 data-src="<?= $medicalServices->getPicture() ?>"
                 alt="Медицинские услуги «НИКА Дент» Брест"
                 data-object-fit="scale-down"
                 data-object-position="50% 50%">
            <article class="text-center">
                <h2 class="grad"><?= $medicalServices->title ?></h2>
                <p class="lead text-lead">
                    <?= $medicalServices->description ?>
                </p>
            </article>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                        <rect class="arrow-outter" x="3" width="1" height="10"/>
                    </svg>
                </a>
            </div>
        </div>

        <?php foreach ($model as $item) {?>
        <div class="section" id="s-<?= $item->id?>-title">
            <div class="section-content section-content-bottom">
                <img src="<?= $item->getPicture() ?>" data-src="<?= $item->getPicture() ?>" alt="<?= $item->title?> в медицинском центре «НИКА Дент» Брест" data-object-fit="cover" data-object-position="50% 50%">
                <article class="">
                    <div class="container-bottom">
                        <div class="brushes">
                            <div class="brush brush-white">
                                <h3><?= $item->title?></h3>
                            </div>
                            <p class="hidden-xs hidden-sm hidden-md hidden-lg hidden-xl">Стоматологический кабинет в медицинском центре НИКА Дент в Бресте. Услуги стоматолога в Бресте. Стоматология в Бресте.
                                Терапевтическая стоматология в Бресте: лечение кариеса ICON, эндодонтическое лечение эндомотором, эстетическая реставрация зубов, проофессиональная гигиена полости рта УЗ-скейлером, профессиональная гигиена полости рта системой PROPHYFLEX.
                                Ортопедическая стоматология в Бресте: безметалловая керамика за один визит по технологии CEREC, протезирование на каркасе из диоксида циркония, протезирование на имплантах, керамические виниры, микропротезирование: керамические вкладки и накладки, металлокерамические коронки и мостовидные протезы, все виды съемных протезов.
                                Парадонтологическая стоматология в Бресте: периохирургия, профилактика и лечение заболеваний периодонта DURR DENTAL VECTOR, Профилактическая гигиена полости рта УЗ-скейлером и PROPHYFLEX, шинирование зубов, обучение гигиене полости рта, подбор средств гигиены по уходу за полостью рта с учетом индивидуальных особенностей.
                                Хирургическая стоматология в Бресте: Зубосохраняющие операции, максимально безболезненное удаление зубов с сохранением тканей и подготовкой к имплантации, удаление новообразований костной и мягкой тканей полости рта, пластика уздечки губ и языка, периохирургия, изготовление и использование в лечении тромбоцитарной аутоплазмы.
                                Детская стоматология в Бресте: подготовка к первому визиту к стоматологу, обучение гигиене полости рта, подбор средств гигиены для ухода за полостью рта с учетом индивидуальных особенностей, профилактика кариеса, лечение временных и постоянных зубов, спасение травмированного зуба, удаление временных и постоянных зубов, пластика уздечки губ и языка, ортодонтическое лечение: пластинки, трейнеры, раннее лечение брекет-системой.
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="quotes-block">
                                    <div class="quotespad"><?= $item->quote?></div>
                                    <div class="quotespad-em"><?= $item->author?></div>
                                </div>
                            </div>
                        </div>
                        <div class="link-more link-more-inner hidden-sm-down">
                            <a class="btn btn-primary btn-square moveSectionDown" href="#dentist-desc">
                                <svg class="icon icon-arrow-down" width="7" height="32">
                                    <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                                    <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                                    <rect class="arrow-outter" x="3" width="1" height="10"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </article>
            </div>

        </div>

        <div class="section" id="s-<?= $item->id?>-desc">
            <?php
            $category = \common\models\Category::find()->where(['parent_id' => $item->category_id])->all();
            if (!$category) {
                $category = \common\models\Category::find()->where(['id' => $item->category_id])->all();
            }
            ?>
            <?php foreach ($category as $key=>$cat) { ?>
            <div class="slide" id="d-slide-<?= $key?>">
                <div class="row">
                    <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                        <article class="content-desc">
                            <h3><?= $cat->name?></h3>
                            <?php if ($cat->introtext) {?>
                                <p><?= $cat->introtext?></p>
                            <? } ?>
                            <ul><?php if ($cat->pages) {?>
                                    <?php foreach ($cat->getPages()->orderBy(['sort' => SORT_ASC])->all() as $page) { ?>
                                        <?php if (!$page->subcategory_id) {?>
                                        <li>
                                            <?php if ($page->active) {
                                                echo \yii\helpers\Html::a($page->title,
                                                    ['/pages/index', 'slug' => $page->slug],
                                                    ['target' => '_self', 'title' => $page->title]);
                                                } else {
                                                echo $page->title;
                                            } ?>
                                        </li>
                                        <? } ?>
                                    <? } ?>
                                <? } ?>
                            </ul>
                        </article>
                    </div>
                    <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                        <img src="<?= $cat->getPicture() ?>"
                             data-src="<?= $cat->getPicture() ?>"
                             alt="<?= $cat->name?> в медицинском центре «НИКА Дент» Брест"
                             data-object-fit="scale-down"
                             data-object-position="0% 50%"
                             class="service-preview">
                    </div>
                </div>
            </div>
            <? } ?>
            <?php if (count($category) > 1) { ?>
            <div class="secondary-navigation">
                <div>
                    <a class="btn btn-lg btn-secondary btn-prev moveSlideLeft" href="#">
                        <svg class="icon icon-arrow-left" width="31" height="7">
                            <path class="arrow-inner" d="M1206,1151.5l6,3.5v-7l-6,3.5" transform="translate(-1206 -1148)"/>
                            <rect class="arrow-inner" x="6" y="3" width="15" height="1"/>
                            <rect class="arrow-outter" x="21" y="3" width="10" height="1"/>
                        </svg>
                        <span class="btn-lbl hidden-sm-down">Предыдущий</span>
                    </a>
                </div>
                <div>
                    <a class="btn btn-lg btn-secondary btn-next moveSlideRight" href="#">
                        <svg class="icon icon-arrow-right" width="31" height="7">
                            <path class="arrow-inner" d="M1242,1151.5l-6,3.5v-7l6,3.5" transform="translate(-1211 -1148)"/>
                            <rect class="arrow-inner" x="10" y="3" width="15" height="1"/>
                            <rect class="arrow-outter" y="3" width="10" height="1"/>
                        </svg>
                        <span class="btn-lbl hidden-sm-down">Следующий</span>
                    </a>
                </div>
            </div>
            <? } ?>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                        <rect class="arrow-outter" x="3" width="1" height="10" />
                    </svg>
                </a>
            </div>
        </div>
        <? } ?>
    </div>

</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>

<!-- microdata -->
<div style="display:none;" itemscope itemtype="http://schema.org/Organization">
    <span itemprop="name">Медицинский центр НИКА Дент</span>
    <meta itemprop="url" content="http://nikadent.by/">
    <meta itemprop="logo" content="http://nikadent.by/logo.png">
    <meta itemprop="description" content="Стоматология, рентгенодиагностика, ЛОР, косметология , дерматология, онкология, массаж и реабилитация в медицинском центре НИКА Дент в Бресте.">
    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

    <meta itemprop="addressCountry" content="Беларусь">
    <meta itemprop="addressRegion" content="Брестская область">
    <span itemprop="addressLocality">Брест</span>,
    <span itemprop="streetAddress">Гоголя, 1В</span>,

  </span>
    тел.: <span itemprop="telephone">+375336083727</span>.
    <span  itemprop="location" itemscope itemtype="http://schema.org/Place">
    <meta itemprop="name" content="Медицинский центр НИКА Дент">
    <meta itemprop="telephone" content="+375336083727">
    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
      <meta itemprop="addressCountry" content="Беларусь">
      <meta itemprop="addressRegion" content="Брестская обалсть">
      <meta itemprop="addressLocality" content="г. Брест">
      <meta itemprop="streetAddress" content="ул. Гоголя, 1В">
    </span>
    <span itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
      <meta itemprop="latitude" content="52.08838282202195">
      <meta itemprop="longitude" content="23.675210700247135">
    </span>
  </span>
</div>

<div style="display:none;"><!--
<div class="TabbedPanelsContentGroup vcard">
<span class="fn org">Медицинский центр НИКА Дент</span>:
<div class="adr">
<p>
<span class="postal-code">224030</span>
<abbr class="country-name" title="Беларусь"></abbr>
<abbr class="region" title="Брестская область"></abbr>
<span class="locality">Брест</span>,
<span class="street-address">Гоголя, 1В</span>
</p>
</div>
<span class="url" title="http://nikadent.by"></span>
<abbr class="tel" title="+375336083727"></abbr>
<span class="geo">
<span class="latitude">
<span class="value-title" title="52.08838282202195"></span>
</span>
<span class="longitude">
<span class="value-title" title="23.675210700247135"></span>
</span>
</span>
</div>-->
    <div class="vcard">
        <div>
            <span class="category">Медицинский центр</span>
            <span class="fn org">НИКА Дент</span>
        </div>
        <div class="adr">
            <span class="locality">г. Брест</span>,
            <span class="street-address">Гоголя, 1В</span>
        </div>
        <div>Телефон: <span class="tel">+375 (33) 608-37-27</span></div>
        <div>Мы работаем <span class="workhours">ежедневно с 8:00 до 21:00</span>
            <span class="url">
     <span class="value-title" title="http://nikadent.by"> </span>
   </span>
        </div>
    </div>
</div>

<!-- /microdata -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-89848172-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-89848172-1');
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47331675 = new Ya.Metrika({
                    id:47331675,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<!-- /Yandex.Metrika counter -->