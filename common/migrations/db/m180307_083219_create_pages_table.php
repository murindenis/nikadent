<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m180307_083219_create_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('pages', [
            'id'                          => $this->primaryKey(),
            'category_id'                 => $this->integer()->notNull(),
            'title'                       => $this->string()->notNull()->comment('Название процедуры'),
            'slug'                        => $this->string()->notNull()->comment('ЧПУ'),
            'description'                 => $this->text()->comment('Описание процедуры'),
            'target'                      => $this->text()->comment('Цель'),
            'active_ingredient'           => $this->text()->comment('Активный компонент'),
            'material'                    => $this->text()->comment('Материалы'),
            'treaded_area'                => $this->text()->comment('Обрабатываемые зоны'),
            'indication'                  => $this->text()->comment('Показания'),
            'contraindication'            => $this->text()->comment('Противопоказания'),
            'technique'                   => $this->text()->comment('Методика и этапность процедуры'),
            'difference_from_mesotherapy' => $this->text()->comment('Отличие от мезотерапии'),
            'efficiency'                  => $this->text()->comment('Эффективность'),
            'recommendation'              => $this->text()->comment('Рекомендации до и после процедуры'),
            'result'                      => $this->text()->comment('Результат'),
            'duration_of_procedure'       => $this->text()->comment('Продолжительность процедуры'),
            'created_at'                  => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pages');
    }
}
