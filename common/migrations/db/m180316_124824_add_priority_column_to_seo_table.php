<?php

use yii\db\Migration;

/**
 * Handles adding slug_and_priority to table `seo`.
 */
class m180316_124824_add_priority_column_to_seo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('seo', 'priority', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('seo', 'priority');
    }
}
