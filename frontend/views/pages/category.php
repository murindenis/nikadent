<?php

$this->title = $model->seo_title;

$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $model->seo_description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $model->seo_title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $model->seo_keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $model->seo_description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://nikadent.by/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'    => 'DC.title',
    'content' => $model->seo_title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $model->seo_description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
?>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header-back.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/feedback-popup.php');
    ?>
    <div class="main" id="new__page">
        <div class="" id="article-1">
            <div class="article-section">

                <div class="article-row">
                    <div class="fixed-menu-container">
                        <div class="fixed-menu-wrapper">
                            <ul>
                                <?php foreach ($parentCategory as $item) {?>
                                <li class="main__menu__item">
                                    <div class="fixed-link-list">
                                        <div class="icon-menu">
                                            <div class="fa fa-chevron-down rotate"></div>
                                        </div>
                                        <a href="#" class="fixed-link-drop s-link-red"><?= $item->name?></a>
                                    </div>
                                    <div class="side-sub-menu">
                                        <ul>
                                            <?php foreach ($item->getChildrenCategories($item->id) as $cat) { ?>
                                                <li><a href="/index.php#<?= $cat->slug?>" class="s-menu s-link-sm"><?= $cat->name?></a></li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="row article-title">
                        <h1><?= $model->seo_h1?></h1>
                    </div>
                    <div class="article-side">
                        <div class="row">
                            <div class="col-xs-12 category-desc">
                                <?= $model->description?>
                            </div>
                            <div class="col-xs-12">
                                <form id="category__contact">
                                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
                                    <div class="js-contacts-form">
                                        <h2>Запись на прием</h2>
                                        <fieldset>
                                            <div class="input-group">
                                                <input type="text" name="name" placeholder="Как к вам обратиться?"> <span class="deco"></span></div>
                                            <div class="input-group">
                                                <input type="text" name="phone" placeholder="Как с вами связаться?"> <span class="deco"></span></div>
                                            <div class="input-group">
                                                <?php
                                                $doctors = \common\models\Doctor::find()->asArray()->all();
                                                ?>
                                                <select name="doctor" data-placeholder="Выберите нужного специалиста" class="deco">
                                                    <?php foreach ($doctors as $doctor) { ?>
                                                        <option value="<?= $doctor['id']; ?>"><?= $doctor['name']; ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                            <div class="input-group">
                                                <input type="text" id="category__datepicker" name="date" placeholder="Выберите удобные дату и время приема:">
                                                <span class="deco"></span>
                                            </div>
                                            <div class="input-group">
                                                <textarea type="text" name="comment" placeholder="Вы можете оставить комментарий врачу"></textarea>
                                                <span class="deco"></span>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <footer class="js-contacts-footer">
                                        <button class="btn btn-secondary btn-sm category__submitBtn"><span class="icon icon-lines"><span></span></span> Отправить
                                        </button>
                                    </footer>

                                    <div class="hidden error-message js-contacts-errors"></div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>
