<?php

use yii\db\Migration;

/**
 * Handles the creation of table `doctor`.
 */
class m180305_090224_create_doctor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('doctor', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('doctor');
    }
}
