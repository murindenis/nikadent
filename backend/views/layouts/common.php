<?php
/**
 * @var $this yii\web\View
 */

use backend\assets\BackendAsset;
use backend\models\SystemLog;
use backend\widgets\Menu;
use common\models\TimelineEvent;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\log\Logger;
use yii\widgets\Breadcrumbs;

$bundle = BackendAsset::register($this);
?>
<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
<div class="wrapper">
    <!-- header logo: style can be found in header.less -->
    <header class="main-header">
        <a href="<?php echo Yii::$app->urlManagerFrontend->createAbsoluteUrl('/') ?>" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <?php echo Yii::$app->name ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only"><?php echo Yii::t('backend', 'Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                                 class="user-image">
                            <span><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header light-blue">
                                <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                                     class="img-circle" alt="User Image"/>
                                <p>
                                    <?php echo Yii::$app->user->identity->username ?>
                                    <small>
                                        <?php echo Yii::t('backend', 'Member since {0, date, short}', Yii::$app->user->identity->created_at) ?>
                                    </small>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?php echo Html::a(Yii::t('backend', 'Profile'), ['/sign-in/profile'], ['class' => 'btn btn-default btn-flat']) ?>
                                </div>
                                <div class="pull-left">
                                    <?php echo Html::a(Yii::t('backend', 'Account'), ['/sign-in/account'], ['class' => 'btn btn-default btn-flat']) ?>
                                </div>
                                <div class="pull-right">
                                    <?php echo Html::a(Yii::t('backend', 'Logout'), ['/sign-in/logout'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <?php echo Html::a('<i class="fa fa-cogs"></i>', ['/site/settings']) ?>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                         class="img-circle"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo Yii::t('backend', 'Hello, {username}', ['username' => Yii::$app->user->identity->getPublicIdentity()]) ?></p>
                    <a href="<?php echo Url::to(['/sign-in/profile']) ?>">
                        <i class="fa fa-circle text-success"></i>
                        <?php echo Yii::$app->formatter->asDatetime(time()) ?>
                    </a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?php echo Menu::widget([
                'options' => ['class' => 'sidebar-menu'],
                'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                'activateParents' => true,
                'items' => [
                    [
                        'label' => 'Формы',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Запись на приём',
                        'icon' => '<i class="glyphicon glyphicon-book"></i>',
                        'url' => ['/application-for-doctor/index'],
                        'badge' => \common\models\ApplicationForDoctor::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Отзывы',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/feedback/index'],
                        'badge' => \common\models\Feedback::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Главная',
                        'options' => ['class' => 'header'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Медуслуги',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/medical-services/update?id=1'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Блоки',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/glavnaya/index'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'О нас',
                        'options' => ['class' => 'header'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'О клинике',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/about-box/update?id=1'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Сертификаты',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/certificate/index'],
                        'badge' => \common\models\Certificate::find()->count(),
                        'badgeBgClass' => 'label-success',
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Пациентам',
                        'options' => ['class' => 'header'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Медицинский туризм',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/pacient-box/update?id=1'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => Yii::t('backend', 'System'),
                        'options' => ['class' => 'header'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Врачи',
                        'icon' => '<i class="glyphicon glyphicon-user"></i>',
                        'url' => ['/doctor/index'],
                        'badge' => \common\models\Doctor::find()->count(),
                        'badgeBgClass' => 'label-success',
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Категории',
                        'icon' => '<i class="glyphicon glyphicon-folder-open"></i>',
                        'url' => ['/category/index'],
                        'badge' => \common\models\Category::find()->count(),
                        'badgeBgClass' => 'label-success',
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Статьи',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/pages/index'],
                        'badge' => \common\models\Pages::find()->count(),
                        'badgeBgClass' => 'label-success',
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => Yii::t('backend', 'Pages'),
                        'options' => ['class' => 'header'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],

                    [
                        'label' => 'Цены клиники',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/price/index'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'Контакты',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/contacts/update?id=1'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => 'SEO',
                        'icon' => '<i class="glyphicon glyphicon-file"></i>',
                        'url' => ['/seo/index'],
                        'visible' => Yii::$app->user->can('administrator')
                    ],

                ]
            ]) ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $this->title ?>
                <?php if (isset($this->params['subtitle'])): ?>
                    <small><?php echo $this->params['subtitle'] ?></small>
                <?php endif; ?>
            </h1>

            <?php echo Breadcrumbs::widget([
                'tag' => 'ol',
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if (Yii::$app->session->hasFlash('alert')): ?>
                <?php echo Alert::widget([
                    'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                    'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                ]) ?>
            <?php endif; ?>
            <?php echo $content ?>
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<?php $this->endContent(); ?>
