<div class="submenu row">
    <div class="submenu__col">
        <?php
        $catKosmetology = \common\models\Category::find()->where(['id' => \common\models\Category::CATEGORY_KOSMETOLOGY])->one();
        ?>
        <?= \yii\helpers\Html::a($catKosmetology->name, ['/pages/category', 'slug' => $catKosmetology->slug], ['class' => 'title-link'])?>
        <?php
        $cosmetology = \common\models\Category::find()
            ->where(['parent_id' => \common\models\Category::CATEGORY_KOSMETOLOGY])->all();
        ?>
        <?php foreach ($cosmetology as $item) {?>
            <a href="/index.php#<?= $item->slug?>" class="submenu__a"><?= $item->name?></a>
        <? } ?>
    </div>
    <div class="submenu__col">
        <?php
        $catStomatology = \common\models\Category::find()->where(['id' => \common\models\Category::CATEGORY_STOMATOLOGY])->one();
        ?>
        <?= \yii\helpers\Html::a($catStomatology->name, ['/pages/category', 'slug' => $catStomatology->slug], ['class' => 'title-link'])?>
        <?php
        $stomatology = \common\models\Category::find()
            ->where(['parent_id' => \common\models\Category::CATEGORY_STOMATOLOGY])->all();
        ?>
        <?php foreach ($stomatology as $item) {?>
            <a href="index.php#<?= $item->slug?>" class="submenu__a"><?= $item->name?></a>
        <? } ?>
    </div>
    <div class="submenu__col">
        <?php
        $catReabilitaciya = \common\models\Category::find()->where(['id' => \common\models\Category::CATEGORY_REABILITACIYA])->one();
        ?>
        <?= \yii\helpers\Html::a($catReabilitaciya->name, ['/pages/category', 'slug' => $catReabilitaciya->slug], ['class' => 'title-link'])?>
        <?php
        $reabilitaciya = \common\models\Category::find()
            ->where(['parent_id' => \common\models\Category::CATEGORY_REABILITACIYA])->all();
        ?>
        <?php foreach ($reabilitaciya as $item) {?>
            <a href="/index.php#<?= $item->slug?>" class="submenu__a"><?= $item->name?></a>
        <? } ?>
    </div>
    <div class="submenu__col">
        <?php
        $catRentgen = \common\models\Category::find()->where(['id' => \common\models\Category::CATEGORY_RENTGEN])->one();
        ?>
        <?= \yii\helpers\Html::a($catRentgen->name, ['/pages/category', 'slug' => $catRentgen->slug], ['class' => 'title-link'])?>
        <?php
        $catOtolarigology = \common\models\Category::find()->where(['id' => \common\models\Category::CATEGORY_OTOLARINGOLOGY])->one();
        ?>
        <?= \yii\helpers\Html::a($catOtolarigology->name, ['/pages/category', 'slug' => $catOtolarigology->slug], ['class' => 'title-link'])?>
        <?php
        $catDermatology = \common\models\Category::find()->where(['id' => \common\models\Category::CATEGORY_DERMATOLOGY])->one();
        ?>
        <?= \yii\helpers\Html::a($catDermatology->name, ['/pages/category', 'slug' => $catDermatology->slug], ['class' => 'title-link'])?>
        <?php
        $catOnkology = \common\models\Category::find()->where(['id' => \common\models\Category::CATEGORY_ONKOLOGY])->one();
        ?>
        <?= \yii\helpers\Html::a($catOnkology->name, ['/pages/category', 'slug' => $catOnkology->slug], ['class' => 'title-link'])?>
    </div>
    <div class="submenu__col">
        <p class="title-link">Действующее предложение</p>
        <div class="action">
            <img src="/img/media/service/dentist-p1.png" alt="">
        </div>
    </div>
</div>