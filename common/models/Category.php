<?php

namespace common\models;

use Codeception\PHPUnit\ResultPrinter\HTML;
use himiklab\sitemap\behaviors\SitemapBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $slug
 * @property int $created_at
 * @property string $introtext
 * @property string $description
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $seo_h1
 */
class Category extends \yii\db\ActiveRecord
{
    const CATEGORY_STOMATOLOGY    = 2;
    const CATEGORY_RENTGEN        = 3;
    const CATEGORY_OTOLARINGOLOGY = 4;
    const CATEGORY_KOSMETOLOGY    = 5;
    const CATEGORY_DERMATOLOGY    = 6;
    const CATEGORY_ONKOLOGY       = 7;
    const CATEGORY_REABILITACIYA  = 8;

    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'created_at', 'parent_id']);
                },
                'dataClosure' => function ($model) {
                    if ($model->parent_id == 1) {
                        /** @var self $model */
                        return [
                            'loc'      => Url::to('category/'.$model->slug, true),
                            'lastmod'  => date('c',$model->created_at),
                            'priority' => 0.8
                        ];
                    }
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'created_at'], 'integer'],
            [['name'], 'required'],
            [['name', 'slug', 'introtext'], 'string', 'max' => 255],
            [['picture'], 'safe'],
            [['description'], 'string'],
            [['seo_title', 'seo_keywords', 'seo_description', 'seo_h1'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'parent_id' => Yii::t('common', 'Parent ID'),
            'name' => Yii::t('common', 'Category Name'),
            'slug' => Yii::t('common', 'Slug'),
            'created_at' => Yii::t('common', 'Created At'),
            'introtext' => 'Краткое описание',
            'description' => 'Описание',
            'seo_title' => 'title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
            'seo_h1' => 'H1',
        ];
    }

    public static function build_tree($categoryLink, $cats, $parent_id, $only_parent = false, $submenu = false) {
        if (is_array($cats) && isset($cats[$parent_id])) {
            $tree = '<ul>';
            if ($only_parent==false) {
                foreach ($cats[$parent_id] as $cat) {
                    $catChild = Category::find()->where(['parent_id' => $cat['id']])->count();
                    if ($catChild > 0) {
                        $tree .= '<li>
                                    <div class="s-link-list">
                                        <div class="icon-menu">
                                            <div class="fa fa-chevron-down rotate"></div>
                                        </div>
                                        <a href="#" class="s-link-drop s-link-red">'.$cat['name'].'</a>
                                    </div>
                                    <div class="side-sub-menu">';
                        $tree .=  self::build_tree($categoryLink, $cats, $cat['id'], false, true);
                        $tree .= '</div>
                                </li>';
                    }
                    if (!$catChild) {
                        if ($submenu) {
                            $class = "s-menu s-link-sm";
                        } else {
                            $class = "s-menu s-link-drop s-link-red";
                        }
                        $tree .= '<li>'.
                                    \yii\helpers\Html::a($cat['name'], ['/site/'.$categoryLink , '#' => $cat['slug']], ['class' => $class])
                                 .'</li>';
                    }
                }
            } /*elseif(is_numeric($only_parent)){
                $cat = $cats[$parent_id][$only_parent];
                $tree .= '<li class="'.$parent_id.'">'.$cat['name'].' #'.$cat['id'];
                $tree .=  self::build_tree($cats, $cat['id']);
                $tree .= '</li>';
            }*/
            $tree .= '</ul>';
        }
        else return null;
        return $tree;
    }

    public static function getCategoryColumn() {
        return self::find()
            ->select(['name', 'id'])
            ->indexBy('id')
            ->column();
    }

    public static function getCategoryParentColumn() {
        return self::find()
            ->select(['name', 'id'])
            ->where(['parent_id' => 1])
            ->indexBy('id')
            ->column();
    }

    public function getPicture($default = null)
    {
        return $this->path
            ? Yii::getAlias($this->base_url . '/' . $this->path)
            : $default;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Pages::className(), ['category_id' => 'id']);
    }

    public function getChildrenCategories($id) {
        return $this::find()->where(['parent_id' => $id])->all();
    }
}
