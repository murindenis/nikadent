<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pacient_box`.
 */
class m180314_073816_create_pacient_box_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('pacient_box', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'path'        => $this->string(),
            'base_url'    => $this->string(),
            'created_at'  => $this->integer(),
        ], $tableOptions);

        $this->insert('pacient_box', [
            'title'       => 'МЕДИЦИНСКИЙ ТУРИЗМ В КЛИНИКЕ «НИКА ДЕНТ»',
            'description' => 'Это гарантия безопасности и качества, профессионализма и уверенности врачей, логики и продуманности плана лечения, эстетики и функциональности наших результатов, более 20 лет опыта.',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pacient_box');
    }
}
