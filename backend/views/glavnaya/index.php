<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\IndexSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Блоки на главной';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="index-index">

    <p>
        <?= Html::a('Добавить раздел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            //'category_id',
            'title',
            'quote',
            'author',
            //'path',
            //'base_url:url',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
