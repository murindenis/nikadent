<?php

use yii\db\Migration;

/**
 * Handles adding introtext to table `category`.
 */
class m180412_120315_add_introtext_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'introtext', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('category', 'introtext');
    }
}
