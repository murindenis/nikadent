<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?//= $form->field($model, 'category_id')->dropDownList(\common\models\Category::getCategoryColumn(), ['prompt' => '-']) ?>
            <?php echo $form->field($model, 'category_id')->dropDownList(\common\models\Category::getCategoryColumn(), ['prompt'=>'Выберите категорию',
                    'onchange' => '
                        $.post("/pages/subcategory?id=' . '"+$(this).val(), function (data) {
                            $("select#pages-subcategory_id").html(data);
                        });']);
            ?>

            <?= $form->field($model, 'subcategory_id')->dropDownList($subcategory, ['prompt' => '-']) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options'=>[
                        'minHeight'=>150,
                        'maxHeight'=>400,
                        'buttonSource'=>true,
                        'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            ) ?>

            <p class="p-label">
                <?php $targetClass = $model->target ? '' : 'b-no-active'; ?>
                <button class="btn btn-success target_add <?= !$model->target ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger target_remove <?= !$model->target ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Цель
            </p>
            <div class="target <?= $targetClass ?>">
                <?= $form->field($model, 'target')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $activeClass = $model->active_ingredient ? '' : 'b-no-active'; ?>
                <button class="btn btn-success active_ingredient_add <?= !$model->active_ingredient ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger active_ingredient_remove <?= !$model->active_ingredient ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Активный компонент
            </p>
            <div class="active_ingredient <?= $activeClass?>">
                <?= $form->field($model, 'active_ingredient')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $materialClass = $model->material ? '' : 'b-no-active'; ?>
                <button class="btn btn-success material_add <?= !$model->material ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger material_remove <?= !$model->material ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Материал
            </p>
            <div class="material <?= $materialClass?>">
                <?= $form->field($model, 'material')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $treadedClass = $model->treaded_area ? '' : 'b-no-active'; ?>
                <button class="btn btn-success treaded_area_add <?= !$model->treaded_area ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger treaded_area_remove <?= !$model->treaded_area ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Обрабатываемые зоны
            </p>
            <div class="treaded_area <?= $treadedClass?>">
                <?= $form->field($model, 'treaded_area')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $indicationClass = $model->indication ? '' : 'b-no-active'; ?>
                <button class="btn btn-success indication_add <?= !$model->indication ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger indication_remove <?= !$model->indication ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Показания
            </p>
            <div class="indication <?= $indicationClass?>">
                <?= $form->field($model, 'indication')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $contraindicationClass = $model->contraindication ? '' : 'b-no-active'; ?>
                <button class="btn btn-success contraindication_add <?= !$model->contraindication ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger contraindication_remove <?= !$model->contraindication ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Противопоказания
            </p>
            <div class="contraindication <?= $contraindicationClass?>">
                <?= $form->field($model, 'contraindication')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $techniqueClass = $model->technique ? '' : 'b-no-active'; ?>
                <button class="btn btn-success technique_add <?= !$model->technique ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger technique_remove <?= !$model->technique ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Методика и этапность процедуры
            </p>
            <div class="technique <?= $techniqueClass?>">
                <?= $form->field($model, 'technique')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $efficiencyClass = $model->efficiency ? '' : 'b-no-active'; ?>
                <button class="btn btn-success efficiency_add <?= !$model->efficiency ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger efficiency_remove <?= !$model->efficiency ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Эффективность
            </p>
            <div class="efficiency <?= $efficiencyClass?>">
                <?= $form->field($model, 'efficiency')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $recommendationClass = $model->recommendation ? '' : 'b-no-active'; ?>
                <button class="btn btn-success recommendation_add <?= !$model->recommendation ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger recommendation_remove <?= !$model->recommendation ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Рекомендации до и после процедуры
            </p>
            <div class="recommendation <?= $recommendationClass?>">
                <?= $form->field($model, 'recommendation')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $resultClass = $model->result ? '' : 'b-no-active'; ?>
                <button class="btn btn-success result_add <?= !$model->result ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger result_remove <?= !$model->result ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Результат
            </p>
            <div class="result <?= $resultClass?>">
                <?= $form->field($model, 'result')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video'],
                        'options'=>[
                            'minHeight'=>150,
                            'maxHeight'=>400,
                            'buttonSource'=>true,
                            'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                )->label(false) ?>
            </div>

            <p class="p-label">
                <?php $durationClass = $model->duration_of_procedure ? '' : 'b-no-active'; ?>
                <button class="btn btn-success duration_of_procedure_add <?= !$model->duration_of_procedure ? '' : 'b-no-active' ?>">
                    <i class="glyphicon glyphicon-plus-sign"></i>
                </button>
                <button class="btn btn-danger duration_of_procedure_remove <?= !$model->duration_of_procedure ? 'b-no-active' : '' ?>">
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </button>
                Продолжительность процедуры
            </p>
            <div class="duration_of_procedure <?= $durationClass?>">
                <?= $form->field($model, 'duration_of_procedure')->textInput()->label(false) ?>
            </div>
            <?= $form->field($model, 'active')->checkbox();?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">SEO</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput()->label('Title') ?>
            <?= $form->field($model, 'seo_description')->textInput()->label('Description') ?>
            <?= $form->field($model, 'seo_keywords')->textInput()->label('Keywords') ?>
            <?= $form->field($model, 'seo_h1')->textInput()->label('H1') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
