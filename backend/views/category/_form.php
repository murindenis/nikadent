<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
        <?= $form->field($model, 'parent_id')->dropDownList(\common\models\Category::getCategoryColumn(), ['prompt' => '-']) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'introtext')->textInput(['maxlength' => true])->label('Краткое описание') ?>

        <?= $form->field($model, 'description')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video'],
                'options'=>[
                    'minHeight'=>350,
                    'maxHeight'=>600,
                    'buttonSource'=>true,
                    'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>

        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($model, 'picture')->widget(
            \trntv\filekit\widget\Upload::classname(),
            [
                'url' => ['avatar-upload']
            ]
        )->label('Изображение')?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">SEO</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput()->label('Title') ?>
            <?= $form->field($model, 'seo_description')->textInput()->label('Description') ?>
            <?= $form->field($model, 'seo_keywords')->textInput()->label('Keywords') ?>
            <?= $form->field($model, 'seo_h1')->textInput()->label('H1') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
