<?php

namespace frontend\assets;

use common\assets\Html5shiv;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Nikadent application asset
 */
class NikadentAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $css = [
        'nikadent/stylesheets/global.css',
        'nikadent/stylesheets/jquery.datetimepicker.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css'
    ];

    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js',
        'nikadent/javascripts/vendors/jquery.fullPage.js',
        'nikadent/javascripts/vendors/scrolloverflow.js',
        'nikadent/javascripts/content.js',
        'nikadent/javascripts/vendors/jquery.datetimepicker.js',
        'nikadent/javascripts/vendors/jquery.easings.min.js',


    ];
}
