<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\ApplicationForDoctor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-for-doctor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'doctor_id')->dropDownList($model->getDoctorColumn(), ['prompt' => '-']) ?>

    <?
    echo DateTimePicker::widget([
        'name' => 'date',
        'options' => ['placeholder' => 'Дата приёма'],
        'value' => date('d-M-y h:i', $model->date),
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'd-M-y h:i',
            'startDate' => '01-Mar-2014 12:00 AM',
            'todayHighlight' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
