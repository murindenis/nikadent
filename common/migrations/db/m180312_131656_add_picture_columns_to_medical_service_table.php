<?php

use yii\db\Migration;

/**
 * Handles adding picture to table `medical_services`.
 */
class m180312_131656_add_picture_columns_to_medical_service_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('medical_services', 'path', $this->string());
        $this->addColumn('medical_services', 'base_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('medical_services', 'path');
        $this->dropColumn('medical_services', 'base_url');
    }
}
