<?php

namespace frontend\controllers;

use common\models\AboutBox;
use common\models\ApplicationForDoctor;
use common\models\Category;
use common\models\Certificate;
use common\models\Contacts;
use common\models\Feedback;
use common\models\Index;
use common\models\MedicalServices;
use common\models\Price;
use frontend\models\ContactForm;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $medicalServices = MedicalServices::findOne(1);
        $model = Index::find()->all();

        return $this->render('index', [
            'medicalServices' => $medicalServices,
            'model' => $model,
        ]);
    }

    public function actionPacients()
    {
        return $this->render('pacients');
    }

    public function actionAbout()
    {
        $aboutBox = AboutBox::findOne(1);
        $certificate = Certificate::find()->all();

        return $this->render('about', [
            'aboutBox' => $aboutBox,
            'certificate' => $certificate,
        ]);
    }

    public function actionFeedback()
    {
        $model = Feedback::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('feedback', [
            'model' => $model
        ]);
    }
    public function actionPrice()
    {
        $prices = \common\models\Price::find()->all();
        $mas = \common\models\Category::getCategoryParentColumn();
        return $this->render('price', [
            'prices' => $prices,
            'mas' => $mas,
        ]);
    }
    public function actionContacts()
    {
        $model = Contacts::find()->where(['id' => 1])->one();
        return $this->render('contacts', [
            'model' => $model
        ]);
    }

    public function actionFormAjax()
    {
        $name    = Yii::$app->request->post('name');
        $phone   = Yii::$app->request->post('phone');
        $doctor  = Yii::$app->request->post('doctor');
        $date    = Yii::$app->request->post('date');
        $comment = Yii::$app->request->post('comment');

        $appForDoctor = new ApplicationForDoctor();
        $appForDoctor->username  = $name;
        $appForDoctor->phone     = $phone;
        $appForDoctor->doctor_id = $doctor;
        $appForDoctor->date      = strtotime($date);
        $appForDoctor->comment   = $comment;
        $appForDoctor->save();

        /*$doctorName = Doctor::find()->where(['id' => (int)$doctor])->one();

            $msg = '<b>'.date('d-m-Y H:i:s').'</b>- поступила запись на приём<br/>'.
                   'Имя: '.$name.'<br/>'.
                   'Телефон: '.$phone.'<br/>'.
                   'Запись к врачу: '.$doctorName->name.'<br/>'.
                   'Запись на дату: '.$date.'<br/>'.
                   'Комментарий:'.$comment.'<br/>';

            $appForDoctor->sendEmail('nikadent@gmail.com', 'nikadent@gmail.com', 'Запись на приём', $msg);*/
        $doctorName = \common\models\Doctor::find()->where(['id' => (int)$doctor])->one();

        $msg = '<b>'.date('d-m-Y H:i:s').'</b>- поступила запись на приём<br/>'.
            'Имя: '.$name.'<br/>'.
            'Телефон: '.$phone.'<br/>'.
            'Запись к врачу: '.$doctorName->name.'<br/>'.
            'Запись на дату: '.$date.'<br/>'.
            'Комментарий:'.$comment.'<br/>';

        $subject = 'Запись на приём';
        $to	 = 'nikadent@gmail.com';
        $headers= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        mail($to, $subject, $msg, $headers);
    }

    public function actionFeedbackAjax()
    {
        $name    = Yii::$app->request->post('name');
        $phone   = Yii::$app->request->post('phone');
        $theme   = Yii::$app->request->post('title');
        $comment = Yii::$app->request->post('text');

        $feedback = new Feedback();
        $feedback->name  = $name;
        $feedback->phone = $phone;
        $feedback->theme = $theme;
        $feedback->text  = $comment;
        $feedback->save();
    }
}
