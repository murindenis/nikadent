<?php

use yii\db\Migration;

/**
 * Handles adding sort to table `pages`.
 */
class m180419_090317_add_sort_column_to_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('pages', 'sort', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('pages', 'sort');
    }
}
