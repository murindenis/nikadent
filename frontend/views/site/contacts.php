<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/seo.php');
?>
    <h1 style="display:none;">
        <?= $seo->h1 ?>
    </h1>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    ?>

    <div class="main">
        <div class="section section-content section-content-bottom" id="s-contact">
            <img src="/img/map/map-substract.png" data-src="/img/map/map-substract.png" alt="" data-object-position="10% 90%" class="map-substract">
            <img src="/img/map/nikamap.png" data-src="/img/map/nikamap.png" alt="" data-object-fit="contain" data-object-position="50% 50%">
            <article class="">
                <div class="container-bottom">
                    <div class="brushes">
                        <div class="brush brush-white">
                            <h3>Расположение</h3>
                        </div>
                    </div>
                </div>
            </article>
            <div class="map-contacts">
                <div class="map-contact">
                    <div class="map-contact__content">

                        <h3>Контакты</h3>
                        <hr>
                        <div class="m-contact">
                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-phone"></div>
                                </div>
                                <a href="tel:<?= $model->phone_house?>" class="m-phone-link"><?= $model->phone_house?></a>
                            </div>
                        </div>
                        <div class="m-contact">
                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-mobile"></div>
                                </div>
                                <a href="tel:<?= $model->phone_mob1?>"><?= $model->phone_mob1?></a>
                            </div>
                        </div>
                        <div class="m-contact">
                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-mobile"></div>
                                </div>
                                <a href="tel:<?= $model->phone_mob2?>"><?= $model->phone_mob2?></a>
                            </div>
                        </div>
                        <div class="m-contact hidden-sm-down">
                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-envelope-o"></div>
                                </div>
                                <a href="mailto:<?= $model->email?>"><?= $model->email?></a>
                            </div>
                        </div>
                        <div class="m-contact_block hidden-md-up center-sm">
                            <a href="#detail-contact"><h4>Подробнее</h4></a>
                        </div>
                        <div class="m-contact_block hidden-sm-down">
                            <h3>Адрес</h3>
                            <hr>
                            <div class="m-contact">
                                ул. Гоголя 1 “В”, Брест<br>224030, Беларусь
                            </div>
                        </div>
                        <div class="m-contact_block hidden-sm-down">
                            <h3>Время работы</h3>
                            <hr>
                            <div class="m-contact">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>ПН - СБ&nbsp;</td>
                                        <td><?= $model->work_time?></td>
                                    </tr>
                                    <tr>
                                        <td>ВС&nbsp;</td>
                                        <td>Выходной</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section-content section-content-center hidden-md-up" id="detail-contact">
            <div class="row m-contact-row">
                <div class="col-xs-6">
                    <h3>Контакты</h3>
                </div>
                <div class="col-xs-6">
                    <div class="row m-contact">
                        <div class="col col-xs-12 m-phone">

                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-phone"></div>
                                </div>
                                <a href="tel:+375 162 555517" class="m-phone-link">+375 162 55-55-17</a>
                            </div>

                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-mobile"></div>
                                </div>
                                <a href="tel:+375 44 720 37 27">+375 44 720-37-27</a>
                            </div>

                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-mobile"></div>
                                </div>
                                <a href="tel:+375 33 608 37 27">+375 33 608-37-27</a>
                            </div>

                            <div class="m-phone">
                                <div class="icon-phone">
                                    <div class="fa fa-envelope-o"></div>
                                </div>
                                <a href="mailto:nikadent@gmail.com">nikadent@gmail.com</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-contact-row">
                <div class="col-xs-6">
                    <h3>Адрес</h3>
                </div>
                <div class="col-xs-6 m-contact">
                    ул. Гоголя 1 “В”, Брест<br>224000, Беларусь
                </div>
            </div>
            <div class="row m-contact-row">
                <div class="col-xs-6">
                    <h3>Время работы</h3>
                </div>
                <div class="col-xs-6 m-contact">
                    <table>
                        <tbody>
                        <tr>
                            <td>ПН - СБ&nbsp;</td>
                            <td>8.00 - 21.00</td>
                        </tr>
                        <tr>
                            <td>ВС&nbsp;</td>
                            <td>Выходной</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>