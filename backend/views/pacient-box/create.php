<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PacientBox */

$this->title = Yii::t('backend', 'Create Pacient Box');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Pacient Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pacient-box-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
