<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Seo */

$this->title = 'Редактировать SEO';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Seos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="seo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
