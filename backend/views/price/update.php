<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Price */

$this->title = 'Редактировать цены';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Prices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="price-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
