<aside class="feedbacks-popup hidden animate-slow animate-in-out">
    <img src="/img/media/contacts/background.png" data-src="/img/media/contacts/background.png" alt="Медицинский центр НИКА Дент в Бресте" data-object-fit="scale-down" data-object-position="50% 50%">
    <header class="container-header header-contacts">
        <a class="btn btn-square btn-primary menu-button js-feedbacks-close">
            <svg class="icon icon-close" width="13" height="13">
                <path d="M13.286,14.01 L14.011,13.285 L1.711,0.985 L0.987,1.709 L13.286,14.01 Z M1.715,14.01 L0.991,13.285 L13.291,0.985 L14.016,1.709 L1.715,14.01 Z"></path>
                <path d="M13.286,14.01 L14.011,13.285 L1.711,0.985 L0.987,1.709 L13.286,14.01 Z M1.715,14.01 L0.991,13.285 L13.291,0.985 L14.016,1.709 L1.715,14.01 Z"></path>
            </svg>
        </a>
        <p class="contacts">
            <a href="whatsapp://send?phone=375336083727" class="whatsapp" title="Whatsapp"><span class="whatsapp-text">1234</span></a>
            <a href="viber://add?number=375447203727" class="viber" title="Viber"><span class="viber-text">1234</span></a>
            <span><a href="tel:+375 33 608 37 27">+375 33 608-37-27</a></span>
            <span><a href="tel:+375 44 720 37 27">+375 44 720-37-27</a></span>
            <br><a class="js-contacts-link contacts-link"><em>Запись на прием!</em></a>
        </p>
    </header>
    <form id="comment-form">
        <div class="js-feedbacks-form">
            <h2>Добавить отзыв</h2>
            <fieldset>
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
                <input type="hidden" name="date" value="<?php echo date("Y-m-d H:i:s")?>">
                <div class="input-group">
                    <input type="text" name="name" placeholder="Как к Вам обратиться?"> <span class="deco"></span></div>
                <div class="input-group">
                    <input type="text" name="phone" placeholder="Как с Вами связаться?"> <span class="deco"></span></div>
                <div class="input-group">
                    <input type="text" name="title" placeholder="Вы можете указать тему"> <span class="deco"></span></div>
                <div class="input-group">
                    <textarea type="text" name="text" placeholder="Вы можете оставить отзыв о клинике"></textarea><span class="deco"></span></div>

            </div>
            </fieldset>
            <footer class="js-feedbacks-footer">
                <button class="btn btn-secondary btn-sm js-feedback-btn addComment"><span class="icon icon-lines"><span></span></span> Отправить
                </button>
            </footer>
        </div>
        <div class="hidden error-message js-feedbacks-errors"></div>
    </form>
</aside>