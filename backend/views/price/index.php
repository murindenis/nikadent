<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цены клиники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-index">

    <p>
        <?= Html::a(Yii::t('backend', 'Добавить услугу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'category_id',
                'value'     => 'category.name',
                'filter' => Html::activeDropDownList($searchModel, 'category_id', \common\models\Category::getCategoryParentColumn(), ['prompt' => '-', 'class' => 'form-control']),

            ],
            'name',
            'price',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
