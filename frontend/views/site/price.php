<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/seo.php');
?>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/feedback-popup.php');
    ?>

    <div class="main">
        <div class="" id="s-price">
            <div class="price-section">
                <div class="row price-title">
                    <h1>Цены клиники "НИКА Дент"</h1>
                </div>
                <div class="price-row">
                    <?php foreach ($mas as $key=>$item) { ?>
                        <?php
                            $count = \common\models\Price::find()->where(['category_id' => $key])->count();
                        ?>
                    <div class="price-side">
                        <?php if ($count > 0) {?>
                        <div class="row price-row-title"><?= $item?></div>
                        <? } ?>
                        <?php foreach ($prices as $price) { ?>
                            <?php if ($item == $price->category->name) {?>
                            <div class="row">
                                <div class="col-xs-10"><?= $price->name?></div>
                                <div class="col-xs-2"><?= $price->price?> руб</div>
                            </div>
                            <? } ?>
                        <? } ?>
                    </div>
                    <? } ?>
                    <div>
                    </div>
                </div>
            </div>
</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>
