<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Index */

$this->title = 'Добавить раздел';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Indices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="index-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
