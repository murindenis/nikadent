<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Certificate */

$this->title = 'Добавить сертификат';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Certificates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="certificate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
