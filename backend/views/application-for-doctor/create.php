<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApplicationForDoctor */

$this->title = Yii::t('backend', 'Create Application For Doctor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Application For Doctors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-for-doctor-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
