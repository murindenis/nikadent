<?php

use yii\db\Migration;

/**
 * Handles the creation of table `certificate`.
 */
class m180314_081945_create_certificate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('certificate', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(),
            'path'       => $this->string(),
            'base_url'   => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('certificate');
    }
}
