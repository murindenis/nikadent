<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $address
 * @property string $phone_house
 * @property string $phone_mob1
 * @property string $phone_mob2
 * @property string $email
 * @property string $work_time
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'phone_house', 'phone_mob1', 'phone_mob2', 'email', 'work_time'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'address' => Yii::t('common', 'Address'),
            'phone_house' => Yii::t('common', 'Phone House'),
            'phone_mob1' => Yii::t('common', 'Phone Mob1'),
            'phone_mob2' => Yii::t('common', 'Phone Mob2'),
            'email' => Yii::t('common', 'Email'),
            'work_time' => Yii::t('common', 'Work Time'),
        ];
    }
}
