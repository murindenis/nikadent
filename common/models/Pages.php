<?php

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property int $category_id
 * @property int $subcategory_id
 * @property string $title Название процедуры
 * @property string $slug ЧПУ
 * @property string $description Описание процедуры
 * @property string $target Цель
 * @property string $active_ingredient Активный компонент
 * @property string $material Материалы
 * @property string $treaded_area Обрабатываемые зоны
 * @property string $indication Показания
 * @property string $contraindication Противопоказания
 * @property string $technique Методика и этапность процедуры
 * @property string $difference_from_mesotherapy Отличие от мезотерапии
 * @property string $efficiency Эффективность
 * @property string $recommendation Рекомендации до и после процедуры
 * @property string $result Результат
 * @property string $duration_of_procedure Продолжительность процедуры
 * @property int $created_at
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_h1
 * @property string $sort
 * @property int $active
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class'        => SluggableBehavior::className(),
                'attribute'    => 'title',
                'ensureUnique' => true,
                'immutable'    => true
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'created_at', 'active']);
                },
                'dataClosure' => function ($model) {
                    if ($model->active) {
                        /** @var self $model */
                        return [
                            'loc'      => Url::to('pages/'.$model->slug, true),
                            'lastmod'  => date('c',$model->created_at),
                            'priority' => 0.6
                        ];
                    }
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'slug'], 'required'],
            [['active'], 'boolean'],
            [['category_id', 'subcategory_id', 'created_at', 'sort'], 'integer'],
            [['description', 'target', 'active_ingredient', 'material', 'treaded_area', 'indication', 'contraindication', 'technique', 'difference_from_mesotherapy', 'efficiency', 'recommendation', 'result', 'duration_of_procedure'], 'string'],
            [['title', 'slug', 'seo_title', 'seo_description', 'seo_keywords', 'seo_h1'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'category_id' => Yii::t('common', 'Parent ID'),
            'subcategory_id' => 'Подкатегория',
            'title' => Yii::t('common', 'Title'),
            'slug' => Yii::t('common', 'Slug'),
            'description' => Yii::t('common', 'Description'),
            'target' => Yii::t('common', 'Target'),
            'active_ingredient' => Yii::t('common', 'Active Ingredient'),
            'material' => Yii::t('common', 'Material'),
            'treaded_area' => Yii::t('common', 'Treaded Area'),
            'indication' => Yii::t('common', 'Indication'),
            'contraindication' => Yii::t('common', 'Contraindication'),
            'technique' => Yii::t('common', 'Technique'),
            'difference_from_mesotherapy' => Yii::t('common', 'Difference From Mesotherapy'),
            'efficiency' => Yii::t('common', 'Efficiency'),
            'recommendation' => Yii::t('common', 'Recommendation'),
            'result' => Yii::t('common', 'Result'),
            'duration_of_procedure' => Yii::t('common', 'Duration Of Procedure'),
            'created_at' => Yii::t('common', 'Created At'),
            'seo_title' => Yii::t('common', 'Title'),
            'seo_description' => Yii::t('common', 'Description'),
            'seo_keywords' => Yii::t('common', 'Keywords'),
            'seo_h1' => Yii::t('common', 'h1'),
            'sort' => 'Порядок',
            'active' => 'Активно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategories()
    {
        return $this->hasMany(Pages::className(), ['subcategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(Pages::className(), ['id' => 'subcategory_id']);
    }

    public static function getPagesColumn() {
        return self::find()
            ->select(['title', 'id'])
            ->indexBy('id')
            ->column();
    }
}
