<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Seo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'page')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('title') ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true])->label('keywords') ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true])->label('description') ?>

    <?= $form->field($model, 'h1')->textInput(['maxlength' => true])->label('h1') ?>

    <?= $form->field($model, 'priority')->textInput(['maxlength' => true])->label('priority') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
