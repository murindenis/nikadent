<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutBox */

$this->title = 'О клинике';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'About Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="about-box-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
