<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'slug') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'target') ?>

    <?php // echo $form->field($model, 'active_ingredient') ?>

    <?php // echo $form->field($model, 'material') ?>

    <?php // echo $form->field($model, 'treaded_area') ?>

    <?php // echo $form->field($model, 'indication') ?>

    <?php // echo $form->field($model, 'contraindication') ?>

    <?php // echo $form->field($model, 'technique') ?>

    <?php // echo $form->field($model, 'difference_from_mesotherapy') ?>

    <?php // echo $form->field($model, 'efficiency') ?>

    <?php // echo $form->field($model, 'recommendation') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'duration_of_procedure') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
