<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pages;

/**
 * PagesSearch represents the model behind the search form of `common\models\Pages`.
 */
class PagesSearch extends Pages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'sort', 'subcategory_id', 'active'], 'integer'],
            [['title', 'slug', 'description', 'target', 'active_ingredient', 'material', 'treaded_area', 'indication', 'contraindication', 'technique', 'difference_from_mesotherapy', 'efficiency', 'recommendation', 'result', 'duration_of_procedure'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'subcategory_id' => $this->subcategory_id,
            'created_at' => $this->created_at,
            'sort' => $this->sort,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'target', $this->target])
            ->andFilterWhere(['like', 'active_ingredient', $this->active_ingredient])
            ->andFilterWhere(['like', 'material', $this->material])
            ->andFilterWhere(['like', 'treaded_area', $this->treaded_area])
            ->andFilterWhere(['like', 'indication', $this->indication])
            ->andFilterWhere(['like', 'contraindication', $this->contraindication])
            ->andFilterWhere(['like', 'technique', $this->technique])
            ->andFilterWhere(['like', 'difference_from_mesotherapy', $this->difference_from_mesotherapy])
            ->andFilterWhere(['like', 'efficiency', $this->efficiency])
            ->andFilterWhere(['like', 'recommendation', $this->recommendation])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'duration_of_procedure', $this->duration_of_procedure]);

        return $dataProvider;
    }
}
