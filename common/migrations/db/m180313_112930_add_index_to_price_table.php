<?php

use yii\db\Migration;

/**
 * Class m180313_112930_add_index_to_price_table
 */
class m180313_112930_add_index_to_price_table extends Migration
{
    public function up()
    {
        $this->createIndex('idx-price_category_id', '{{%price}}', 'category_id');
        $this->addForeignKey('fk-price_category_id', '{{%price}}', 'category_id', '{{%category}}', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('fk-price_category_id', 'price');
        $this->dropIndex('idx-price_category_id', 'price');
    }
}
