<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "index".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $quote
 * @property string $author
 * @property string $path
 * @property string $base_url
 * @property int $created_at
 *
 * @property Category $category
 */
class Index extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'index';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url'
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'quote'], 'required'],
            [['category_id', 'created_at'], 'integer'],
            [['title', 'quote', 'author', 'path', 'base_url'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['picture'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'category_id' => Yii::t('common', 'Category ID'),
            'title' => Yii::t('common', 'Title'),
            'quote' => Yii::t('common', 'Quote'),
            'author' => Yii::t('common', 'Author'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getPicture($default = null)
    {
        return $this->path
            ? Yii::getAlias($this->base_url . '/' . $this->path)
            : $default;
    }
}
