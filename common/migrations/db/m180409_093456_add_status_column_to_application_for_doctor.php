<?php

use yii\db\Migration;

/**
 * Class m180409_093456_add_status_column_to_application_for_doctor
 */
class m180409_093456_add_status_column_to_application_for_doctor extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('application_for_doctor', 'status', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('application_for_doctor', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180409_093456_add_status_column_to_application_for_doctor cannot be reverted.\n";

        return false;
    }
    */
}
