<?php

use yii\db\Migration;

/**
 * Handles adding picture to table `category`.
 */
class m180312_141246_add_picture_columns_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'path', $this->string());
        $this->addColumn('category', 'base_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('category', 'path');
        $this->dropColumn('category', 'base_url');
    }
}
