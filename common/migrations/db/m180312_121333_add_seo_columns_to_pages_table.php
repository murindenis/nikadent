<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `pages`.
 */
class m180312_121333_add_seo_columns_to_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('pages', 'seo_title', $this->string());
        $this->addColumn('pages', 'seo_description', $this->string());
        $this->addColumn('pages', 'seo_keywords', $this->string());
        $this->addColumn('pages', 'seo_h1', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('pages', 'seo_title');
        $this->dropColumn('pages', 'seo_description');
        $this->dropColumn('pages', 'seo_keywords');
        $this->dropColumn('pages', 'seo_h1');
    }
}
