<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `category`.
 */
class m180518_113934_add_seo_columns_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'seo_title', $this->string());
        $this->addColumn('category', 'seo_description', $this->string());
        $this->addColumn('category', 'seo_keywords', $this->string());
        $this->addColumn('category', 'seo_h1', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('category', 'seo_title');
        $this->dropColumn('category', 'seo_description');
        $this->dropColumn('category', 'seo_keywords');
        $this->dropColumn('category', 'seo_h1');
    }
}
