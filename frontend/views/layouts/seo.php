<?php
$seo = \common\models\Seo::find()->where(['page' => Yii::$app->controller->action->id])->one();
$this->title = $seo->title;

$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $seo->keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $seo->description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $seo->title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $seo->keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $seo->description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://nikadent.by/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'    => 'DC.title',
    'content' => $seo->title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $seo->keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
$this->registerMetaTag([
    'name'    => 'robots',
    'content' => 'noyaca'
]);
