<?php
$this->title = $model->seo_title;

$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $model->seo_description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $model->seo_title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $model->seo_keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $model->seo_description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://nikadent.by/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'    => 'DC.title',
    'content' => $model->seo_title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $model->seo_description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
?>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header-back.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    ?>

    <div class="article-row article-row-fluid">
        <div class="article-fluid-nav">
            <!--  <div class="massage__nav">
                 <ul>
                     <li><a href="#">Косметический массаж лица  и шеи</a></li>
                     <li><a href="#">Пластический массаж лица и шеи</a></li>
                     <li><a href="#">Массаж по Жаке</a></li>
                     <li><a href="#">Массаж волосистой части головы</a></li>
                     <li><a href="#">Массаж рук</a></li>
                 </ul>
             </div> -->
        </div>
        <div class="article-fluid-content">
            <div class="row article-title">
                <h1><?= $model->title?></h1>
            </div>
        </div>
    </div>
    <div class="article-row article-row-fluid">
        <div class="article-fluid-nav">
            <div class="massage__nav">
                <ul class="message__nav__desct">
                    <?php foreach (\common\models\Pages::find()->where(['subcategory_id' => $model->id])->all() as $page) {?>
                    <li><a href="<?= \yii\helpers\Url::toRoute(['/pages/index', 'slug' => $page->slug])?>"><?= $page->title?></a></li>
                    <? } ?>
                </ul>
                <div class="button__more">

                    <div class="s-link-list">
                        <div class="icon-menu">
                            <div class="fa fa-chevron-down rotate"></div>
                        </div>
                        <a href="#!" class="s-link-drop s-link-red">Другие</a>
                    </div>
                    <div class="side-sub-menu">
                        <ul>
                            <li><a class="s-menu s-link-sm" href="">Косметический массаж лица  и шеи</a></li>
                            <li><a class="s-menu s-link-sm" href="">Пластический массаж лица и шеи</a></li>
                            <li><a class="s-menu s-link-sm" href="">Массаж по Жаке</a></li>
                            <li><a class="s-menu s-link-sm" href="">Массаж волосистой части головы</a></li>
                            <li><a class="s-menu s-link-sm" href="">Массаж рук</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <div class="article-fluid-content">
            <div class="article-side">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $model->description?>
                    </div>
                </div>
            </div>
            <? if ($model->target) {?>
                <div class="article-side">
                    <div class="row article-row-title">Цель</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->target?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->active_ingredient) {?>
                <div class="article-side">
                    <div class="row article-row-title">Активный компонент</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->active_ingredient?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->material) {?>
                <div class="article-side">
                    <div class="row article-row-title">Материалы</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->material?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->treaded_area) {?>
                <div class="article-side">
                    <div class="row article-row-title">Обрабатываемые зоны</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->treaded_area?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->indication) {?>
                <div class="article-side">
                    <div class="row article-row-title">Показания</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->indication?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->contraindication) {?>
                <div class="article-side">
                    <div class="row article-row-title">Противопоказания</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->contraindication?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->technique) {?>
                <div class="article-side">
                    <div class="row article-row-title">Методика и этапность процедуры</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->technique?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->efficiency) {?>
                <div class="article-side">
                    <div class="row article-row-title">Эффективность</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->efficiency?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->recommendation) {?>
                <div class="article-side">
                    <div class="row article-row-title">Рекомендации до и после процедуры</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->recommendation?>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? if ($model->result) {?>
                <div class="article-side">
                    <div class="row article-row-title">Результат</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $model->result?>
                            <br>
                            <hr class="hr-article">
                            <p>Продолжительность процедуры <?= $model->duration_of_procedure?></p>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
    </div>
</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>