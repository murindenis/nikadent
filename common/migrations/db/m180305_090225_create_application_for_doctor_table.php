<?php

use yii\db\Migration;

/**
 * Handles the creation of table `application_for_doctor`.
 */
class m180305_090225_create_application_for_doctor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('application_for_doctor', [
            'id' => $this->primaryKey(),
            'username'   => $this->string()->notNull(),
            'phone'      => $this->string()->notNull(),
            'doctor_id'  => $this->integer()->notNull(),
            'date'       => $this->integer()->notNull(),
            'comment'    => $this->text(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-application_for_doctor_doctor_id', '{{%application_for_doctor}}', 'doctor_id');
        $this->addForeignKey('fk-application_for_doctor_doctor_id', '{{%application_for_doctor}}', 'doctor_id', '{{%doctor}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-application_for_doctor_doctor_id','application_for_doctor');
        $this->dropIndex('idx-application_for_doctor_doctor_id','application_for_doctor');
        $this->dropTable('application_for_doctor');
    }
}
