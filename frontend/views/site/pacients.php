<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/seo.php');
?>
    <h1 style="display:none;">
        <?= $seo->h1 ?>
    </h1>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    ?>

    <div id="pacientStory">
        <div class="section" id="s-medtourism-0">
            <div class="section-content section-content-center">
                <img src="/img/media/contacts/background.png" data-src="/img/media/contacts/background.png" alt="" data-object-fit="scale-down" data-object-position="50% 50%">
                <article class="text-center">
                    <h2 class="grad">Медицинский туризм в клинике «НИКА&nbsp;Дент»</h2>
                    <p class="lead text-lead">
                        Это гарантия безопасности и качества, профессионализма и уверенности врачей, логики и продуманности плана лечения, эстетики и функциональности наших результатов, более 20 лет опыта.
                    </p>
                </article>
                <div class="link-more link-more-inner hidden-sm-down">
                    <a class="btn btn-primary btn-square moveSectionDown" href="#">
                        <svg class="icon icon-arrow-down" width="7" height="32">
                            <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                            <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                            <rect class="arrow-outter" x="3" width="1" height="10"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="section" id="s-medtourism-1">
            <div class="row">
                <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                    <article class="content-desc">
                        <h3>Медицинский туризм с преимуществами:</h3>
                        <ul>
                            <li>Команда врачей, инновационное оборудование и передовые методики работают на ваш результат</li>
                            <li>Гарантия инфекционной безопасности лечения, конфиденциальности и правовой безопасности</li>
                            <li>Широкие возможности выбора индивидуального плана лечения<br> по срокам и бюджету</li>
                            <li>Максимально комфортные условия лечения</li>
                            <li>Комплексная online консультация при подаче заявки</li>
                            <li>Помощь в оформлении  документов и билетов</li>
                            <li>Организация услуг трансфера, проживания и  заказа любых услуг по туризму у местных туроператоров</li>
                        </ul>
                    </article>
                </div>
                <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                    <img src="/img/media/pacients/med-p1.png" data-src="/img/media/pacients/med-p1.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%" class="service-preview">
                </div>
            </div>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                        <rect class="arrow-outter" x="3" width="1" height="10" />
                    </svg>
                </a>
            </div>
        </div>

        <div class="section" id="s-medtourism-2">
            <div class="row">
                <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                    <article class="content-desc">
                        <h3>Порядок обращения в клинику</h3>
                        <div class="content-row">
                            <div class="content-section">
                                <div class="content-numb">
                                    <div class="content-sm">шаг</div>
                                    <div class="content-lg">01</div>
                                </div>
                                <div class="content-title">Отправьте заявку с прикрепленным 2D или 3D снимком на email <a href="mailto:nikadent@gmail.com">nikadent@gmail.com</a></div>
                                <div class="icon-chevron">
                                    <div class="fa fa-chevron-down rotate"></div>
                                </div>
                            </div>
                            <div class="content-text">
                                <p>В письменном запросе укажите цель Вашего визита — профилактический осмотр, лечение/диагностика. Опишите как можно более подробно, что Вас беспокоит.</p>
                                <p>Приложите к письму файлы с заключениями врачей/снимками/историю болезни, если таковые имеются. Это поможет нам более точно подобрать Вам личный курс диагностики и лечения.</p>
                                <p>Ваши документы хранятся в строго конфиденциальном порядке.</p>
                            </div>
                        </div>
                        <div class="content-row">
                            <div class="content-section">
                                <div class="content-numb">
                                    <div class="content-sm">шаг</div>
                                    <div class="content-lg">02</div>
                                </div>
                                <div class="content-title">Бесплатная консультация!</div>
                                <div class="icon-chevron">
                                    <div class="fa fa-chevron-down rotate"></div>
                                </div>
                            </div>
                            <div class="content-text">
                                <p>После отправления, на Ваш электронный адрес приходит подтверждение о получении нами запроса.</p>
                                <p>В зависимости от специфики и сложности Вашего случая мы подоберем врача для решения Вашей задачи, основываясь на клинической ситуации и пожеланиях к лечению.</p>
                                <p>Ответ с подробным списком рекомендуемых процедур, плана лечения и счётом-сметой Вы получите в течение 2 дней. </p>
                            </div>
                        </div>
                        <div class="content-row">
                            <div class="content-section">
                                <div class="content-numb">
                                    <div class="content-sm">шаг</div>
                                    <div class="content-lg">03</div>
                                </div>
                                <div class="content-title"><b>Мы занимаемся организацией Вашего приезда</b></div>
                                <div class="icon-chevron">
                                    <div class="fa fa-chevron-down rotate"></div>
                                </div>
                            </div>
                            <div class="content-text">
                                <p>Предоставляем трансфер</p>
                                <p>Подбираем вариант размещения</p>
                                <p>Подготавливаем необходимые документы (включая визовую поддержку)</p>
                            </div>
                        </div>
                        <div class="content-row">
                            <div class="content-section">
                                <div class="content-numb">
                                    <div class="content-sm">шаг</div>
                                    <div class="content-lg">04</div>
                                </div>
                                <div class="content-title"><b>Индивидуальное обследование</b></div>
                                <div class="icon-chevron">
                                    <div class="fa fa-chevron-down rotate"></div>
                                </div>
                            </div>
                            <div class="content-text">
                                <p>Вы проходите комплексную диагностику:</p>
                                <p>консультация врачей специалистов, при небходимости, консультация врачей смежных направлений</p>
                                <p>лабораторные исследования</p>
                                <p>инструментарные исследования: компьютерная томография головы и придаточных пазух носа, эндоскопическое исследование ЛОР-органов, маммография</p>
                                <p>Мы обязательно учтем результаты обследований, сделанных Вами по месту жительства.</p>
                            </div>
                        </div>
                        <div class="content-row">
                            <div class="content-section">
                                <div class="content-numb">
                                    <div class="content-sm">шаг</div>
                                    <div class="content-lg">05</div>
                                </div>
                                <div class="content-title"><b>Эффективное лечение</b></div>
                                <div class="icon-chevron">
                                    <div class="fa fa-chevron-down rotate"></div>
                                </div>
                            </div>
                            <div class="content-text">
                                <p>Вы проходите комплексную диагностику:</p>
                                <ul>
                                    <li>консультация врачей специалистов, при небходимости, консультация врачей смежных направлений</li>
                                    <li>лабораторные исследования</li>
                                    <li>инструментарные исследования: компьютерная томография головы и придаточных пазух носа, эндоскопическое исследование ЛОР-органов, маммография</li>
                                </ul>
                                <p>Мы обязательно учтем результаты обследований, сделанных Вами по месту жительства.</p>
                            </div>
                        </div>
                        <div class="content-row">
                            <div class="content-section">
                                <div class="content-numb">
                                    <div class="content-sm">шаг</div>
                                    <div class="content-lg">06</div>
                                </div>
                                <div class="content-title"><b>Возвращение домой</b></div>
                                <div class="icon-chevron">
                                    <div class="fa fa-chevron-down rotate"></div>
                                </div>
                            </div>
                            <div class="content-text">
                                <p>Вы здоровы и возвращаетесь домой, а мы остаемся на связи с вами. Наши врачи контролируют процесс Вашей дальнейшей адаптации, проводя бесплатные консультации удобным для Вас способом: телефон, эл.почта, Viber.</p>
                                <p>В течение всего процесса переписки, мы всегда рады ответить на любые Ваши вопросы.</p>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                    <img src="/img/media/pacients/med-p1.png" data-src="/img/media/pacients/med-p1.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%" class="service-preview">
                </div>
                <div class="link-more link-more-inner hidden-sm-down">
                    <a class="btn btn-primary btn-square moveSectionDown" href="#">
                        <svg class="icon icon-arrow-down" width="7" height="32">
                            <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                            <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                            <rect class="arrow-outter" x="3" width="1" height="10" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="section" id="s-medinsurance-title">
            <div class="section-content section-content-bottom">
                <img src="/img/media/chapters/p-ch-2.jpg" data-src="/img/media/chapters/p-ch-2.jpg" alt="" data-object-fit="cover" data-object-position="50% 50%">
                <article class="">
                    <div class="container-bottom">
                        <div class="brushes">
                            <div class="brush brush-white">
                                <h3>Медицинское страхование</h3>
                            </div>
                        </div>
                        <div class="link-more link-more-inner hidden-sm-down">
                            <a class="btn btn-primary btn-square moveSectionDown" href="#">
                                <svg class="icon icon-arrow-down" width="7" height="32">
                                    <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                                    <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                                    <rect class="arrow-outter" x="3" width="1" height="10"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <div class="section section-content-center" id="s-medinsurance-0">
            <img src="/img/media/contacts/background.png" data-src="/img/media/contacts/background.png" alt="" data-object-fit="scale-down" data-object-position="50% 50%">
            <article class="text-center">
                <h2 class="grad">Добровольное медицинское страхование</h2>
                <p class="lead text-lead">
                    Современный способ получить качественную многопрофильную медицинскую помощь взрослым и детям в медицинском центре «НИКА&nbsp;Дент»
                </p>
            </article>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                        <rect class="arrow-outter" x="3" width="1" height="10"/>
                    </svg>
                </a>
            </div>
        </div>

        <div class="section section-content-center" id="s-medinsurance-1">
            <div class="row">
                <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                    <article class="content-desc">
                        <h3>ПОРЯДОК ОКАЗАНИЯ УСЛУГ ВЛАДЕЛЬЦАМ <br>ПОЛИСОВ ДМС</h3>
                        <p>Для получения медицинских услуг в нашей клинике владельцу полиса ДМС достаточно обратиться в свою страховую компанию, заключившую договор с медицинским центром «НИКА&nbsp;Дент», где ему выдадут соответствующее направление. По окончании лечения согласно «реестру предоставленных услуг» клиника выставит счет страховой компании.</p>
                    </article>

                </div>
                <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                    <img src="/img/media/pacients/dms-p1.png" data-src="/img/media/pacients/dms-p1.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%" class="service-preview">
                </div>
            </div>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                        <rect class="arrow-outter" x="3" width="1" height="10" />
                    </svg>
                </a>
            </div>
        </div>

        <div class="section section-content-center" id="s-medinsurance-2">
            <div class="row">
                <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                    <article class="content-desc">
                        <h3>ГАРАНТИИ «НИКА ДЕНТ»</h3>
                        <p>Медицинский центр «НИКА Дент» гарантирует каждому своему клиенту обеспечение абсолютной безопасности здоровья на протяжении всего процесса лечения.</p>
                        <p>Обращаясь к нам, вы можете быть уверены в том, что компетентные врачи нашего центра составят для вас индивидуальный план лечения, благодаря чему страховая медицина запомнится Вам не только современными технологиями, но и высоким уровенем качества медицинских услуг благодаря жёсткому контролю качества лечения.</p>
                    </article>
                </div>
                <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                    <img src="/img/media/pacients/dms-p2.png" data-src="/img/media/pacients/dms-p2.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%" class="service-preview">
                </div>
            </div>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                        <rect class="arrow-outter" x="3" width="1" height="10" />
                    </svg>
                </a>
            </div>
        </div>

        <div class="section" id="s-corpo-title">
            <div class="section-content section-content-bottom">
                <img src="/img/media/chapters/p-ch-3.jpg" data-src="/img/media/chapters/p-ch-3.jpg" alt="" data-object-fit="cover" data-object-position="50% 50%">
                <article class="">
                    <div class="container-bottom">
                        <div class="brushes">
                            <div class="brush brush-white">
                                <h3>Корпоративное обслуживание</h3>
                            </div>
                        </div>
                        <div class="link-more link-more-inner hidden-sm-down">
                            <a class="btn btn-primary btn-square moveSectionDown" href="#">
                                <svg class="icon icon-arrow-down" width="7" height="32">
                                    <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                                    <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                                    <rect class="arrow-outter" x="3" width="1" height="10"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <div class="section section-content-center" id="s-corpo-1">
            <img src="/img/media/contacts/background.png" data-src="/img/media/contacts/background.png" alt="" data-object-fit="scale-down" data-object-position="50% 50%">
            <article class="text-center">
                <h2 class="grad">Комплексное медицинское обслуживание</h2>
                <p class="lead text-lead">
                    Медицинский центр «НИКА&nbsp;Дент» рад предложить компаниям и организациям, заботящихся о здоровье сотрудников и руководства, заключение договоров на комплексное медицинское обслуживание
                </p>
            </article>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                        <rect class="arrow-outter" x="3" width="1" height="10"/>
                    </svg>
                </a>
            </div>
        </div>

        <div class="section section-content-center" id="corpo-2">
            <article class="content-desc">
                <div class="row">
                    <div class="col col-xl-6 col-lg-6 col-md-12 col-xs-12 brick-block">
                        <div class="brick-title">
                            <div class="brick"></div>
                            <h3>ПРОГРАММА НЕМАТЕРИАЛЬНОГО СТИМУЛИРОВАНИЯ</h3>
                        </div>
                        <p>Поощрите ценных сотрудников лечением в клинике «НИКА&nbsp;Дент», что даст Вам гарантию их лояльности и экономию на налогах.</p>
                    </div>
                    <div class="col col-xl-6 col-lg-6 col-md-12 col-xs-12 brick-block">
                        <div class="brick-title">
                            <div class="brick"></div>
                            <h3>УЧЕТ И КОНТРОЛЬ</h3>
                        </div>
                        <p>Мы ведем персональный учет медицинских услуг, оказанных сотрудникам Вашей организации, ежемесячно предоставляя отчет в виде акта выполненных работ, по результатам которого выставляется счет.</p>
                    </div>
                    <div class="col col-xl-6 col-lg-6 col-md-12 col-xs-12 brick-block">
                        <div class="brick-title">
                            <div class="brick"></div>
                            <h3>ВЫГОДНАЯ АЛЬТЕРНАТИВА ДМС</h3>
                        </div>
                        <p>Заключая договор напрямую с клиникой, Вы не переплачиваете за услуги страховой компании, а оплачиваете только лечение.</p>
                    </div>
                    <div class="col col-xl-6 col-lg-6 col-md-12 col-xs-12 brick-block">
                        <div class="brick-title">
                            <div class="brick"></div>
                            <h3>ГАРАНТИЯ ИНФЕКЦИОННОЙ БЕЗОПАСНОСТИ</h3>
                        </div>
                        <p>Профессиональная конфиденциальность является частью нашего профессионального этического кодекса.</p>
                    </div>
                </div>
            </article>
            <div class="link-more link-more-inner hidden-sm-down">
                <a class="btn btn-primary btn-square moveSectionDown" href="#">
                    <svg class="icon icon-arrow-down" width="7" height="32">
                        <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)" />
                        <rect class="arrow-inner" x="3" y="10" width="1" height="16" />
                        <rect class="arrow-outter" x="3" width="1" height="10" />
                    </svg>
                </a>
            </div>
        </div>

        <div class="section" id="gifts-title">
            <div class="section-content section-content-bottom">
                <img src="/img/media/chapters/p-ch-4.jpg" data-src="/img/media/chapters/p-ch-4.jpg" alt="" data-object-fit="cover" data-object-position="50% 50%">
                <article class="">
                    <div class="container-bottom">
                        <div class="brushes">
                            <div class="brush brush-white">
                                <h3>Подарочные сертификаты</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="quotes-block">
                                    <div class="quotespad">«Здоровье, рождающее красоту...»</div>
                                    <div class="quotespad-em">Неизвестный</div>
                                </div>
                            </div>
                        </div>
                        <div class="link-more link-more-inner hidden-sm-down">
                            <a class="btn btn-primary btn-square moveSectionDown" href="#">
                                <svg class="icon icon-arrow-down" width="7" height="32">
                                    <path class="arrow-inner" d="M119.5,613l3.5-6h-7l3.5,6" transform="translate(-116 -581)"/>
                                    <rect class="arrow-inner" x="3" y="10" width="1" height="16"/>
                                    <rect class="arrow-outter" x="3" width="1" height="10"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <div class="section section-content-center" id="gifts-2">
            <div class="row">
                <div class="col col-xl-6 col-lg-5 col-md-12 col-xs-12 section-content section-content-center">
                    <article class="content-desc">
                        <h3>Правила использования подарочного сертификата:</h3>
                        <ul>
                            <li>Подарочный сертификат дает право на получение услуг в клинике «НИКА&nbsp;Дент»согласно действующему прейскуранту</li>
                            <li>Подарочный сертификат не действителен без печати и подписи руководителя клиники</li>
                            <li>Срок действия подарочного сертификата составляет 3 месяца</li>
                            <li>Сертификат не подлежит возврату или обмену на деньги</li>
                            <li>Сертификат не восстанавливается в случае его утери</li>
                            <li>Если сумма, указаная в сертификате, меньше стоимости выбранной медицинской услуги, возможна доплата денежных средств</li>
                        </ul>
                    </article>
                </div>
                <div class="col col-xl-6 col-lg-7 hidden-xs hidden-sm hidden-md section-content">
                    <img src="/img/media/pacients/ps-p2.png" data-src="/img/media/pacients/ps-p2.png" alt="" data-object-fit="scale-down" data-object-position="0% 50%" class="service-preview">
                </div>
            </div>
        </div>
    </div>

</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>