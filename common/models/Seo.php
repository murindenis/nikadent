<?php

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "seo".
 *
 * @property int $id
 * @property string $page
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $h1
 * @property int $created_at
 */
class Seo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['page', 'created_at', 'priority']);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc'      => Url::to($model->page.'.php', true),
                        'lastmod'  => date('c',$model->created_at),
                        'priority' => $model->priority
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['page', 'title', 'keywords', 'description', 'h1'], 'string', 'max' => 255],
            [['priority'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'page' => Yii::t('common', 'Page'),
            'title' => Yii::t('common', 'Title'),
            'keywords' => Yii::t('common', 'Keywords'),
            'description' => Yii::t('common', 'Description'),
            'h1' => Yii::t('common', 'H1'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }
}
