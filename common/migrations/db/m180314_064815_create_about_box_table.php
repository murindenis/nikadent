<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_box`.
 */
class m180314_064815_create_about_box_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('about_box', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'created_at'  => $this->integer(),
        ], $tableOptions);

        $this->insert('about_box', [
            'title'       => 'О КЛИНИКЕ',
            'description' => 'Клиника «НИКА Дент» обладает международным статусом и оказывает медицинские услуги гражданам Республики Беларусь и медицинским туристам разных стран. Чтобы укрепить заработанный за пятнадцать лет авторитет, коллекив клиники постоянно работаем над повышением качества медицинских услуг и расширением направлений. У нас все создано для того, чтобы пациентам было комфортно и уютно.',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('about_box');
    }
}
