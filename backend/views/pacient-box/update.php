<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PacientBox */

$this->title = 'МЕДИЦИНСКИЙ ТУРИЗМ В КЛИНИКЕ «НИКА ДЕНТ»';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Pacient Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="pacient-box-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
