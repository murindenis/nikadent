<?php

use yii\db\Migration;

/**
 * Handles the creation of table `price`.
 */
class m180313_110229_create_price_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('price', [
            'id'          => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name'        => $this->string()->notNull(),
            'price'       => $this->string()->notNull(),
            'created_at'  => $this->integer(),

        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('price');
    }
}
