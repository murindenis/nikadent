<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/seo.php');
?>
    <h1 style="display:none;">
        <?= $seo->h1 ?>
    </h1>
<main class="screen-container">
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/side-navigation.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/header.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/contacts-popup.php');
    require_once(Yii::getAlias('@frontend').'/views/layouts/feedback-popup.php');
    ?>

    <div class="main">
        <div class="" id="s-feedback">
            <div class="feedback-section">
                <div class="row feedback-title ">
                    <h1>Отзывы о клинике "НИКА Дент"</h1>
                </div>
                <div class="row" id="comments">
                    <?php foreach ($model as $feedback) { ?>
                        <div class="comment col-xs-12 col-md-12 col-lg-6 middle-md">
                            <div class="feedback-comment ">
                                <div class="hidden">Тема:<?= $feedback->theme ?></div>
                                <div class="feedback-text"><?= $feedback->text ?></div>
                                <div class="feedback-footer">
                                    <div class="feedback-footer__name"><?= $feedback->name ?></div>
                                    <div class="feedback-footer__date"><?= date('Y-m-d H:i:s', $feedback->created_at)?></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="feedback-navigation">
            <a class="js-feedbacks-link contacts-link">
                <i class="icon-write fa fa-pencil-square-o"></i><em>Оставить отзыв!</em>
            </a>
        </div>
    </div>
</main>

<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/rotate-message.php');
?>