<?php

use yii\db\Migration;

/**
 * Handles adding picture to table `about_box`.
 */
class m180314_070344_add_picture_columns_to_about_box_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('about_box', 'path', $this->string());
        $this->addColumn('about_box', 'base_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('about_box', 'path');
        $this->dropColumn('about_box', 'base_url');
    }
}
