<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Pages;
use Yii;
use yii\web\Controller;

/**
 * Pages controller
 */
class PagesController extends Controller
{
    public function actionIndex($slug)
    {
        $model = Pages::find()->where(['slug' => $slug])->one();
         if (empty($model->subcategories)) {
             return $this->render('index', [
                 'model' => $model
             ]);
         } else {
             return $this->render('subindex', [
                 'model' => $model
             ]);
         }
    }

    public function actionTest($slug)
    {
        $model = Pages::find()->where(['slug' => $slug])->one();
         if (empty($model->subcategories)) {
             return $this->render('test', [
                 'model' => $model
             ]);
         } else {
             return $this->render('subindex', [
                 'model' => $model
             ]);
         }
    }

    public function actionCategory($slug)
    {
        $model = Category::find()->where(['slug' => $slug])->one();
        $parentCategory = Category::find()->where(['parent_id' => 1])->all();

        return $this->render('category', [
                'model' => $model,
                'parentCategory' => $parentCategory,
        ]);
    }
}
