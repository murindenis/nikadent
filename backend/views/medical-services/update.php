<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MedicalServices */

$this->title = Yii::t('backend', 'Medical Services', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Medical Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="medical-services-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
