<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <p>
        <?= Html::a(Yii::t('backend', 'Create Page'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'category_id',
                'value'     => 'category.name',
                'filter' => Html::activeDropDownList($searchModel, 'category_id', \common\models\Category::getCategoryColumn(), ['prompt' => '-', 'class' => 'form-control']),
            ],
            [
                'attribute' => 'subcategory_id',
                'value'     => 'subcategory.title',
                'filter' => Html::activeDropDownList($searchModel, 'subcategory_id', \common\models\Pages::getPagesColumn(), ['prompt' => '-', 'class' => 'form-control']),
            ],
            'title',
            'slug',
            'sort',
            //'description:ntext',
            //'target:ntext',
            //'active_ingredient:ntext',
            //'material:ntext',
            //'treaded_area:ntext',
            //'indication:ntext',
            //'contraindication:ntext',
            //'technique:ntext',
            //'difference_from_mesotherapy:ntext',
            //'efficiency:ntext',
            //'recommendation:ntext',
            //'result:ntext',
            //'duration_of_procedure:ntext',
            'active:boolean',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
